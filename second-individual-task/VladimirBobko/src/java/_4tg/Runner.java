package java._4tg;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by TimeLine on 12.04.2017.
 */
public class FileAnalizator {
    private static List<File> fileList1 = new ArrayList<>();
    private static List<File> fileList2 = new ArrayList<>();
    private static List<String> fileEqual = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String pack1 = scanner.nextLine();
        String pack2 = scanner.nextLine();
//        String size = scanner.nextLine();

        analiz(pack1, fileList1);
        analiz(pack2, fileList2);
        equals(fileList1, fileList2);
        System.out.println(fileEqual);

    }

    private static void analiz(String pack, List<File> files) throws IOException {

        File file = new File(pack);
        if (file.exists()){
            throw new IOException();
        }
        for (File file1 : file.listFiles()) {
            files.add(file1);
        }
        for (File file1 : files) {
            if (file1.isDirectory()) {
                String path = file1.getAbsolutePath();
                files.remove(file1);
                analiz(path, files);
            }
        }
    }

    private static void equals(List<File> files1, List<File> files2) {
        int size = files1.size() > files2.size() ? files2.size() : files1.size();
        for (int i = 0; i < size; i++) {
            if (files1.get(i).getName().toString().equals(files2.get(i).getName().toString())) {
                fileEqual.add(files1.get(i).getAbsolutePath());
            }
        }
    }
}