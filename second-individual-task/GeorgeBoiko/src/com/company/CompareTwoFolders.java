package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by PC on 12.04.2017.
 * Compare two folders; files equals if their fullpath equals
 */
public class CompareTwoFolders {

    public CompareTwoFolders() {
    }

    /**
     * compare two Maps of files and view result
     */
    public void compareTo(String pathFirst, String pathSecond, boolean flagSize) throws FileNotFoundException {
        int position = parsePath(pathFirst, pathSecond);
        Map<String, String> filesOfDirsFirst = getSearchDirs(pathFirst, position, flagSize);
        Map<String, String> filesOfDirsSecond = getSearchDirs(pathSecond, position, flagSize);
        for (Map.Entry entry : filesOfDirsFirst.entrySet()) {
            for (Map.Entry entrySecond : filesOfDirsSecond.entrySet()) {
                if (entry.getKey().equals(entrySecond.getKey())) {
                    LogTools.log(entry.getKey() + " : " + entry.getValue().toString());
                }
            }
        }
    }

    /**
     return Map of files
     */
    private Map<String, String> getSearchDirs(String path, int position, boolean flagSize) throws FileNotFoundException {
        File startingDirectory = new File(path);
        CompareTwoFolders listing = new CompareTwoFolders();
        List<File> files = listing.getFileListing(startingDirectory);
        Map<String, String> filesOfDirs = new HashMap<>();
        for (File file : files) {
            if (file.isFile()) {
               // System.out.println(file);
                String fileString = file.toString();
                fileString = fileString.substring(fileString.indexOf("\\", position));
                if (flagSize) {
                    filesOfDirs.put(fileString, file.getName() + " " + file.length() / 1024 + " kb");
                } else {
                    filesOfDirs.put(fileString, file.getName());
                }
            }
        }

        return filesOfDirs;
    }

    /**Parse path to delete root directory
     */
    private int parsePath(String pathFirst, String pathSecond) {
        int position = 0;
        for (int i = 0; i < pathFirst.length(); i++) {
            if (pathFirst.length() <= pathSecond.length()) {
                if (pathFirst.charAt(i) != pathSecond.charAt(i)) {
                    position = i;
                    position++;
                    break;
                }
            } else {
                position = pathSecond.lastIndexOf("\\");
                break;
            }

        }
        return position;
    }

    /**
     * Recursively walk a directory tree and return a List of all
     * Files found; the List is sorted using File.compareTo().
     *
     * @param aStartingDir is a valid directory, which can be read.
     */
    private List<File> getFileListing(File aStartingDir) throws FileNotFoundException {
        validateDirectory(aStartingDir);
        List<File> result = getFileListingNoSort(aStartingDir);
        Collections.sort(result);
        return result;
    }


    private List<File> getFileListingNoSort(File aStartingDir) throws FileNotFoundException {
        List<File> result = new ArrayList<>();
        File[] filesAndDirs = aStartingDir.listFiles();
        List<File> filesDirs = Arrays.asList(filesAndDirs);
        for (File file : filesDirs) {
            result.add(file); //always add, even if directory
            if (!file.isFile()) {
                //must be a directory
                //recursive call!
                List<File> deeperList = getFileListingNoSort(file);
                result.addAll(deeperList);
            }
        }
        return result;
    }


    /**
     * Directory is valid if it exists, does not represent a file, and can be read.
     */

    private void validateDirectory(File aDirectory) throws FileNotFoundException {
        if (aDirectory == null) {
            throw new IllegalArgumentException("Directory should not be null.");
        }
        if (!aDirectory.exists()) {
            throw new FileNotFoundException("Directory does not exist: " + aDirectory);
        }
        if (!aDirectory.isDirectory()) {
            throw new IllegalArgumentException("Is not a directory: " + aDirectory);
        }
        if (!aDirectory.canRead()) {
            throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
        }
    }
}
