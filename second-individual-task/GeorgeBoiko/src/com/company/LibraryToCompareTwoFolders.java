package com.company;

import java.io.File;
import java.io.IOException;

public class LibraryToCompareTwoFolders {

    public static void main(String[] args) throws IOException {
        // write your code here
        String firstPath = "D:\\alfa";
        String secondPath = "D:\\beta";
        boolean flagSize = false;
        /**
         *depth - numbers of levels subcategory
         */
        int depth = 3;

        File fileFirst = new File(firstPath);
        File fileSecond = new File(secondPath);

        /**
         * Create directories and files
         */
        RandomCreateFoldersAndFiles r =new RandomCreateFoldersAndFiles();
        r.mkDirs(fileFirst,depth);
        r.mkDirs(fileSecond,depth);

        /**
         * Compare two directories
         */
        CompareTwoFolders compareTwoFolders = new CompareTwoFolders();
        compareTwoFolders.compareTo(firstPath, secondPath, flagSize);

    }
}
