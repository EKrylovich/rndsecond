package com.company;

import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by PC on 12.04.2017.
 * This class create random numbers of directions, subdirectories and files
 */
public class RandomCreateFoldersAndFiles {

    private final Random r;

    public RandomCreateFoldersAndFiles() {
        r = new Random();
    }

    public void mkDirs(File root, int depth) throws IOException {
        if (depth == 0) {
            return;
        }
        int countOfDirs = r.nextInt(2) + 2;
        int countOfFiles;
        for (int i = 1; i < countOfDirs; i++) {
            File file = new File(root, "a" + i);
            String path = root + "\\a" + i;
            file.mkdir();
            countOfFiles = r.nextInt(2) + 2;
            File files;
            for (int j = 1; j < countOfFiles; j++) {
                files = new File(path + "\\file" + j + ".txt");
                files.createNewFile();
            }
            mkDirs(file, depth - 1);
        }
    }
}
