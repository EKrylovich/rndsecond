package by.fortegroup.eCommerceLab.task2.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by akloster on 4/12/2017.
 */
public class FindFiles {
    public List<File> getFiles(File aStartingDir) throws FileNotFoundException {
        validateDirectory(aStartingDir);
        List<File> result = getFilesNoSort(aStartingDir);
        Collections.sort(result);
        return result;
    }
    private List<File> getFilesNoSort(File aStartingDir) throws FileNotFoundException {
        List<File> result = new ArrayList<>();
        File[] filesAndDirs = aStartingDir.listFiles();
        List<File> filesDirs = Arrays.asList(filesAndDirs);
        for(File file : filesDirs) {
            if(file.isFile())
                result.add(file);
            if (! file.isFile()) {
                List<File> deeperList = getFilesNoSort(file);
                result.addAll(deeperList);
            }
        }
        return result;
    }

    public List<File> compareFiles(List<File> files1, List<File> files2, Boolean withSize ){
        if(withSize)
            return compareWithSize(files1, files2);
        else
            return compareWithoutSize(files1, files2);

    }
    private List<File> compareWithSize(List<File> files1, List<File> files2){
        List<File> output = new ArrayList<>();
        for(File file2 : files2) {
            for(File file1: files1)
                if(file1.getName().equals(file2.getName()) && file1.length() == file2.length())
                    output.add(file1);

        }
        return output;
    }
    private List<File> compareWithoutSize(List<File> files1, List<File> files2){
        List<File> output = new ArrayList<>();
        for(File file2 : files2) {
            for(File file1: files1)
                if(file1.getName().equals(file2.getName()))
                    output.add(file1);

        }
        return output;
    }



    /**
     * Directory is valid if it exists, does not represent a file, and can be read.
     */
    private void validateDirectory (
            File aDirectory
    ) throws FileNotFoundException {
        if (aDirectory == null) {
            throw new IllegalArgumentException("Directory should not be null.");
        }
        if (!aDirectory.exists()) {
            throw new FileNotFoundException("Directory does not exist: " + aDirectory);
        }
        if (!aDirectory.isDirectory()) {
            throw new IllegalArgumentException("Is not a directory: " + aDirectory);
        }
        if (!aDirectory.canRead()) {
            throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
        }
    }
}
