package by.fortegroup.eCommerceLab.task2;

import by.fortegroup.eCommerceLab.task1.files.FindFiles;
import by.fortegroup.eCommerceLab.task1.logger.LogTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        LogTools.log("Enter path 1");
        File folder1 = new File(sc.next());
        LogTools.log("Enter path 2");
        File folder2 = new File(sc.next());
        LogTools.log("Compare with/without size (1/0)");
        int i = sc.nextInt();
        boolean withSize = true;
        if(i==0) {
            withSize = false;
        }
        FindFiles findFiles = new FindFiles();
        List<File> output = new ArrayList<>();
        try {
            output = findFiles.compareFiles(findFiles.getFiles(folder1), findFiles.getFiles(folder2), withSize);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for(File file : output){
            System.out.println(file);
        }
    }
}
