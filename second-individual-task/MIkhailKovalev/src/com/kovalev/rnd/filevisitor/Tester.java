package com.kovalev.rnd.filevisitor;

import com.kovalev.common.logger.Logger;
import com.kovalev.common.logger.SimpleLogger;
import com.kovalev.rnd.filevisitor.comparator.DirectoryCompareProcessor;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Mikhail Kovalev
 */
public class Tester {
	public static final String FIRST_PATH = "test/test1";
	public static final String SECOND_PATH = "test/test2";

	public static void main(String[] args) throws IOException {
		Logger logger = new SimpleLogger();
		DirectoryCompareProcessor directoryComparator = new DirectoryCompareProcessor();

		logger.logInfo("Compare " +FIRST_PATH + " to " + SECOND_PATH + " by name");
		List<File> resultsFiles = directoryComparator.compareDirectoriesByName(FIRST_PATH, SECOND_PATH);
		for (File currentFile : resultsFiles){
			logger.logInfo(currentFile.getPath());
		}

		logger.logInfo("");
		logger.logInfo("Compare " +FIRST_PATH + " to " + SECOND_PATH + " by name and size");
		resultsFiles = directoryComparator.compareDirectoriesByNameAndSize(FIRST_PATH, SECOND_PATH);
		for (File currentFile : resultsFiles){
			logger.logInfo(currentFile.getPath());
		}
	}

	private Tester() {
		throw new UnsupportedOperationException("This class should not be initialized.");
	}
}
