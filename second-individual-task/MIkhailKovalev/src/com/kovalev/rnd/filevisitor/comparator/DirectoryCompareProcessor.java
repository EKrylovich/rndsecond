package com.kovalev.rnd.filevisitor.comparator;

import java.io.File;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Mikhail Kovalev
 */
public class DirectoryCompareProcessor {

	public List<File> compareDirectoriesByName(String pFirstDirectory, String pSecondDirectory){
		List<Comparator<File>> comparators = new LinkedList<>();
		comparators.add(new FileNameComparator());

		return compareDirectories(pFirstDirectory, pSecondDirectory, comparators);
	}

	public List<File> compareDirectoriesByNameAndSize(String pFirstDirectory, String pSecondDirectory){
		List<Comparator<File>> comparators = new LinkedList<>();
		comparators.add(new FileNameComparator());
		comparators.add(new FileSizeComparator());

		return compareDirectories(pFirstDirectory, pSecondDirectory, comparators);
	}

	private List<File> compareDirectories(String pFirstDirectory, String pSecondDirectory, List<Comparator<File>> pComparatorList){
		List<File> resultsFiles = new LinkedList<>();
		File firstDirectory = new File(pFirstDirectory);
		File secondDirectory = new File(pSecondDirectory);
		if (firstDirectory.isDirectory() && secondDirectory.isDirectory()){
			compareDirectories(firstDirectory, secondDirectory, pComparatorList, resultsFiles);
		}
		return resultsFiles;
	}

	private void compareDirectories(File pFirstDir, File pSecondDir,
	                                List<Comparator<File>> pComparatorList, List<File> pResultList){
		File[] firstDirFiles = pFirstDir.listFiles();
		for (File firstDirFile : firstDirFiles){
			File secondDirFile = new File(pSecondDir.getPath() + "/" + firstDirFile.getName());
			if (secondDirFile.exists()){
				if (firstDirFile.isDirectory()){
					compareDirectories(firstDirFile, secondDirFile, pComparatorList, pResultList);
				} else {
					boolean result = true;
					for (Comparator<File> comparator : pComparatorList){
						result = result & (comparator.compare(firstDirFile, secondDirFile) == 0);
					}
					if (result){
						pResultList.add(firstDirFile);
					}
				}
			}
		}
	}

}
