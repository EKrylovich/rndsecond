package com.kovalev.rnd.filevisitor.comparator;

import java.io.File;
import java.util.Comparator;

/**
 * @author Mikhail Kovalev
 */
public class FileSizeComparator implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return (int)(o1.length() - o2.length());
	}
}
