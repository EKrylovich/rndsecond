package com.kovalev.rnd.filevisitor.comparator;

import java.io.File;
import java.util.Comparator;

/**
 * @author Mikhail Kovalev
 */
public class FileNameComparator implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		return o1.getName().equals(o2.getName())?0:1;
	}
}
