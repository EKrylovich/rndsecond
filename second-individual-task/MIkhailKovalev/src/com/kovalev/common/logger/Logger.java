package com.kovalev.common.logger;

/**
 * @author Mikhail Kovalev
 */
public interface Logger {
	/**
	 * Method logs info message
	 *
	 * @param pMessage log message
	 */
	void logInfo(String pMessage);

	/**
	 * Method logs error with message
	 *
	 * @param pMessage error message
	 * @param pThrowable throwable
	 */
	void logError(String pMessage, Throwable pThrowable);

	/**
	 * Method logs error
	 *
	 * @param pThrowable throwable
	 */
	void logError(Throwable pThrowable);
}
