package com.kovalev.common.logger;

/**
 * Logger for logging to System.out
 *
 * @author Mikhail Kovalev
 */
public class SimpleLogger implements Logger{

	/**
	 * Method logs info message to System.out
	 *
	 * @param pMessage log message
	 */
	@Override
	public void logInfo(String pMessage) {
		System.out.println(pMessage);
	}

	/**
	 * Method logs error stacktrace with message to System.out
	 *
	 * @param pMessage error message
	 * @param pThrowable throwable
	 */
	@Override
	public void logError(String pMessage, Throwable pThrowable) {
		if (pThrowable != null){
			System.out.println(pMessage);
			pThrowable.printStackTrace();
		}
	}

	/**
	 * Method logs error stacktrace to System.out
	 *
	 * @param pThrowable throwable
	 */
	@Override
	public void logError(Throwable pThrowable) {
		logError("", pThrowable);
	}
}
