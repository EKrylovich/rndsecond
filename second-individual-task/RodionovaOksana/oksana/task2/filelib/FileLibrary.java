package oksana.task2.filelib;

import oksana.task2.LogTool;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * Creates directory tree with random files amount.
 * Compares files by name or by name and size.
 */

public class FileLibrary {
    private static final String DIRECTORY_NAME_TEMPLATE = "dir";
    private static final String FILE_NAME_TEMPLATE = "file";
    private static final String FILE_EXT = ".txt";
    private static final int MAX_FILES_AMOUNT = 5;
    private int dirAmount;

    public FileLibrary(){
        dirAmount = 0;
    }

    private void createFiles(String pPath){
        File curDirectory = new File(pPath);
        if(!isDirectoryValid(curDirectory)){
            LogTool.errorLog(null, "Directory isn't valid!");
        }
        else{
            final int filesAmount = (int)(Math.random() * MAX_FILES_AMOUNT);
            int filesCreated = 0;
            while(filesCreated <= filesAmount){
                File newFile = new File(pPath + FILE_NAME_TEMPLATE + filesCreated + FILE_EXT);
                if(!newFile.exists()){
                    try {
                        newFile.createNewFile();
                    } catch (IOException e) {
                        LogTool.errorLog(e, "Can't create file!");
                    }
                }
            }
        }
    }

    public void createDirectoryTree(String pPath){
        File newDir = new File(pPath + DIRECTORY_NAME_TEMPLATE + dirAmount);
        if(!newDir.exists()){
            if(!newDir.mkdir()){
                LogTool.errorLog(null, "Directory" + newDir.getName() + "wasn't created!");
            }
            dirAmount++;
        }
        createFiles(newDir.getPath());
        createDirectoryTree(newDir.getPath());
    }

    private boolean isDirectoryValid(File pDir){
        boolean isValid = true;
        if(!pDir.isDirectory()){
            isValid = false;
        }
        else
            if(!pDir.exists()){
                isValid = false;
            }

        return isValid;
    }

    public List<File> compareDirectories(String pPath1, String pPath2, boolean considerSize){
        List<File> filesList1 = getFilesList(pPath1);
        List<File> filesList2 = getFilesList(pPath2);
        List<File> equalFiles = new ArrayList<>();
        for(File file : filesList1){
            if(filesList2.contains(file)){
                if(considerSize){
                    int fileIndex = filesList2.lastIndexOf(file);
                    if(file.length() == filesList2.get(fileIndex).length()){
                        equalFiles.add(file);
                    }
                }
                else{
                    equalFiles.add(file);
                }
            }
        }

        return equalFiles;
    }

    private List<File> getFilesList(String pPath){
        File curDir = new File(pPath);
        List<File> filesList = null;
        if(!isDirectoryValid(curDir)) {
            LogTool.errorLog(null, "Directory isn't valid!");
        }
        else{
            filesList = new ArrayList<>();
            List<File> tempList = new ArrayList<>(Arrays.asList(curDir.listFiles()));
            for(File file : tempList){
                if(file.isFile()){
                    filesList.add(file);
                }
                else{
                    filesList.addAll(getFilesList(file.getPath()));
                }
            }
        }

        return filesList;
    }
}
