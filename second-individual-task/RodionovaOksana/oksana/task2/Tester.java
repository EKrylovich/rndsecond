package oksana.task2;

import oksana.task2.filelib.FileLibrary;

import java.io.File;
import java.util.List;

/**
 * Executes test cases for FileLibrary
 * */

public class Tester {

    public static void main(String[] args){
        FileLibrary fileLibrary = new FileLibrary();
        fileLibrary.createDirectoryTree("\\dirs");
        fileLibrary.createDirectoryTree("\\dirs\\dirs2");
        List<File> list1 = fileLibrary.compareDirectories("\\dirs", "\\dirs\\dirs2", false);
        List<File> list2 = fileLibrary.compareDirectories("\\dirs", "\\dirs\\dirs2", true);

        for(File file : list1){
            LogTool.infoLog(file.getPath());
        }

        for (File file : list2){
            LogTool.infoLog(file.getPath());
        }
    }
}
