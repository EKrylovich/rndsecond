package com.forte;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by forte on 12.4.17.
 */
public class MyFileVisitor extends SimpleFileVisitor<Path> {


    private final List<MyFile> foundFiles = new ArrayList<>();

    public List<MyFile> getFoundFiles() {
        return foundFiles;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        FileVisitResult result = super.visitFile(file, attrs);

        foundFiles.add(new MyFile(file, Files.size(file)));

        return result;
    }
}
