package com.forte;

import java.nio.file.Path;

/**
 * Created by forte on 12.4.17.
 */
public class MyFile {
    private Path file;
    private long size;

    public MyFile(Path file, long size) {
        this.file = file;
        this.size = size;
    }

    public Path getFile() {
        return file;
    }

    public long getSize() {
        return size;
    }
}
