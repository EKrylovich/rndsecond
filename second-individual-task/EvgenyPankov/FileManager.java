package com.forte;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by forte on 12.4.17.
 */
public class FileManager {

    public static void main(String[] args) throws IOException {
        //Create fake directory structure

        Files.createDirectories(Paths.get("/home/forte/folder1/folder2"));
        Files.createDirectories(Paths.get("/home/forte/folder2/folder2"));

        Files.createFile(Paths.get("/home/forte/folder1/file1.txt"));
        Files.createFile(Paths.get("/home/forte/folder2/file1.txt"));
        Files.createFile(Paths.get("/home/forte/folder1/file2.txt"));
        Files.createFile(Paths.get("/home/forte/folder2/file2.txt"));
        Files.createFile(Paths.get("/home/forte/folder1/folder2/file2.txt"));
        Files.createFile(Paths.get("/home/forte/folder2/folder2/file2.txt"));

        MyFileVisitor fileVisitor1 = new MyFileVisitor();
        MyFileVisitor fileVisitor2 = new MyFileVisitor();

        Files.walkFileTree(Paths.get("/home/forte/folder1"), fileVisitor1);
        Files.walkFileTree(Paths.get("/home/forte/folder2"), fileVisitor2);

        List<MyFile> list1 = fileVisitor1.getFoundFiles();
        List<MyFile> list2 = fileVisitor2.getFoundFiles();

        List<MyFile> filesWithTheSameNames = new ArrayList<>();
        List<MyFile> filesWithTheSameNamesAndSize = new ArrayList<>();

        ComparatorByName comparatorByName = new ComparatorByName();
        ComparatorByNameAndSize comparatorByNameAndSize = new ComparatorByNameAndSize();


        for (MyFile file1 : list1) {
            for (MyFile file2 : list2) {

                if (comparatorByName.compare(file1, file2) == 0) {
                    filesWithTheSameNames.add(file1);
                }
            }
        }

        for (MyFile file1 : list1) {
            for (MyFile file2 : list2) {
                if (comparatorByNameAndSize.compare(file1, file2) == 0) {
                    filesWithTheSameNamesAndSize.add(file1);
                }
            }
        }

        LogTools.log("List of identical files by Name");
        for (MyFile file : filesWithTheSameNames) {
            LogTools.log(file.getFile().toString());
        }

        LogTools.log("list of identical files by Name and Size");
        for (MyFile file : filesWithTheSameNamesAndSize) {
            LogTools.log(file.getFile().toString());
        }


    }

    private static class ComparatorByName implements Comparator<MyFile> {
        @Override
        public int compare(MyFile o1, MyFile o2) {
            String relativeFileName1 = o1.getFile().relativize(Paths.get("/home/forte/folder1")).toString();
            String relativeFileName2 = o2.getFile().relativize(Paths.get("/home/forte/folder2")).toString();
            return relativeFileName1.compareTo(relativeFileName2);
        }
    }

    private static class ComparatorByNameAndSize implements Comparator<MyFile> {
        @Override
        public int compare(MyFile o1, MyFile o2) {

            String relativeFileName1 = o1.getFile().relativize(Paths.get("/home/forte/folder1")).toString();
            String relativeFileName2 = o2.getFile().relativize(Paths.get("/home/forte/folder2")).toString();

            return relativeFileName1.compareTo(relativeFileName2) > 0 ? Integer.MAX_VALUE
                    : relativeFileName1.compareTo(relativeFileName2) < 0 ?
                    Integer.MIN_VALUE : o1.getSize() - o2.getSize() > 0 ?
                    Integer.MAX_VALUE : o1.getSize() - o2.getSize() < 0 ?
                    Integer.MIN_VALUE : 0;
        }
    }
}
