package com.fortegr.task2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Comparison {

	public boolean compareToByName(String path1, String path2) throws IOException {
		File folder1 = new File(path1);
		File folder2 = new File(path2);
		
		validateDirectory(folder1);
		validateDirectory(folder1);
		List<File> list1 = getFileListing(folder1);
		
		List<File> list2 = getFileListing(folder2);
		System.out.println(findSame(list1, list2).toString());
		
		return true;		
	}
	
	private List<File> findSame(List<File> list1, List<File> list2) {
		List<File> resultList = new ArrayList<File>();
		for (File file : list1) {
			if ( list2.contains(file)) {
				resultList.add(file);
			}
		}
		return resultList;
	}
	
	private List<File> getFileListing(File dir) throws FileNotFoundException {
	    List<File> result = new ArrayList<>();
	    File[] filesAndDirs = dir.listFiles();
	    List<File> filesDirs = Arrays.asList(filesAndDirs);
	    for(File file : filesDirs) {
	      if (! file.isFile()) {
	        List<File> deeperList = getFileListing(file);
	        result.addAll(deeperList);
	      } else {
	    	  result.add(file);
	      }
	    }
	    return result;
	}
	
	private void validateDirectory (File aDirectory) throws FileNotFoundException {
	    if (aDirectory == null) {
	      throw new IllegalArgumentException("Directory should not be null.");
	    }
	    if (!aDirectory.exists()) {
	      throw new FileNotFoundException("Directory does not exist: " + aDirectory);
	    }
	    if (!aDirectory.isDirectory()) {
	      throw new IllegalArgumentException("Is not a directory: " + aDirectory);
	    }
	    if (!aDirectory.canRead()) {
	      throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
	    }
	  }
}
