package by.tarasov.model.logic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Checker {


    List<File> resultList = new ArrayList<>();
    private File firstDir;
    private File secondDir;
    private boolean flag;

    public Checker( File firstDir, File secondDir, boolean flag) {

        this.firstDir = firstDir;
        this.secondDir = secondDir;
        this.flag = flag;
    }

    public void compare() {

        File[] firstFiles = firstDir.listFiles();
        File[] secondFiles = secondDir.listFiles();
        List<File> firstTempList = new ArrayList<>();
        List<File> secondTempList = new ArrayList<>();

        if (!flag) {
            firstTempList.addAll(insideChecker(firstDir));
            secondTempList.addAll(insideChecker(secondDir));
        }
    }

    private static List<File> insideChecker(File file) {

        List<File> files = new ArrayList<>();
        File[] deepFiles = file.listFiles();
        for (File infile : files) {

            if (!file.isDirectory()) {
                insideChecker(infile);
            } else {
                files.add(infile);
            }

        }
        return files;
    }
}
