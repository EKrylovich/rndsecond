package by.burov.secondTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Simple Logger class for log to cnsole some information
 * @author Alex Burov
 * @version 1.0
 */
public final class Logger {
    private final static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    /**
     * This method print param string to console
     * @param stringToLog
     */
    public static void log(String stringToLog){
        System.out.println(stringToLog);
    }
}
