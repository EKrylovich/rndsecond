package by.burov.secondTask;

import java.io.File;
import java.io.IOException;

/**
 * Created by alex on 12.4.17.
 */
public class Main {


    public static void main(String[] args) throws IOException {

        mkFilesAndDirs();

    }



    private static void mkFilesAndDirs() throws IOException {
        File file = new File("/home/alex/documentation1/other/secretFiles");
        File someFile = new File("/home/alex/documentation1/File1.txt");
        someFile.createNewFile();
        Logger.log(String.valueOf(file.mkdirs()));
        File seconFile = new File("/home/alex/documentation2/other/secretFiles");
        File someSecondFile = new File("/home/alex/documentation2/File.txt");
        someSecondFile.createNewFile();
        Logger.log(String.valueOf(seconFile.mkdirs()));
    }
}
