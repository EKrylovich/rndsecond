package by.fortegroup.secondtask;

import by.fortegroup.secondtask.fileutil.FileManager;

import java.io.IOException;

public class Main {

    public static void main(String[] args)  {
        try {
            FileManager.compareFolders("D:\\folder1", "D:\\folder2", false);
            FileManager.compareFolders("D:\\folder1", "D:\\folder2", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
