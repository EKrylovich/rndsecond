package by.fortegroup.secondtask.fileutil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class FileManager {
    private FileManager() {
    }

    public static void compareFolders(String firstFolderPath, String secondFolderPath, boolean checkFileSize) throws IOException {
        File firstDirectory = new File(firstFolderPath);
        File secondDirectory = new File(secondFolderPath);
        List<File> firstDirectoryFiles = getFileListing(firstDirectory);
        List<File> secondDirectoryFiles = getFileListing(secondDirectory);
        List<File> common = new ArrayList<>();
        for (File firstDirFile : firstDirectoryFiles) {
            for (File secondDirFile : secondDirectoryFiles) {

                String fistFile = firstDirFile.getAbsolutePath().substring(firstDirFile.getAbsolutePath()
                        .indexOf(firstDirectory.getAbsolutePath()) + firstDirectory.getAbsolutePath().length());
                String secondFile = secondDirFile.getAbsolutePath().substring(secondDirFile.getAbsolutePath()
                        .indexOf(secondDirectory.getAbsolutePath()) + secondDirectory.getAbsolutePath().length());
                if (fistFile.equals(secondFile)) {
                    if (checkFileSize && firstDirFile.length() == secondDirFile.length())
                        common.add(firstDirFile);
                }
            }
        }


        for (File file : common) {
            System.out.println(file.getAbsolutePath());
        }
    }

    private static List<File> getFileListing(
            File aStartingDir
    ) throws FileNotFoundException {
        validateDirectory(aStartingDir);
        List<File> result = getFileListingNoSort(aStartingDir);
        Collections.sort(result);
        return result;
    }


    private static List<File> getFileListingNoSort(
            File aStartingDir
    ) throws FileNotFoundException {
        List<File> result = new ArrayList<>();
        File[] filesAndDirs = aStartingDir.listFiles();
        List<File> filesDirs = Arrays.asList(filesAndDirs);
        for (File file : filesDirs) {
            result.add(file);
            if (!file.isFile()) {
                List<File> deeperList = getFileListingNoSort(file);
                result.addAll(deeperList);
            }
        }
        return result;
    }

    private static void validateDirectory(
            File aDirectory
    ) throws FileNotFoundException {
        if (aDirectory == null) {
            throw new IllegalArgumentException("Directory should not be null.");
        }
        if (!aDirectory.exists()) {
            throw new FileNotFoundException("Directory does not exist: " + aDirectory);
        }
        if (!aDirectory.isDirectory()) {
            throw new IllegalArgumentException("Is not a directory: " + aDirectory);
        }
        if (!aDirectory.canRead()) {
            throw new IllegalArgumentException("Directory cannot be read: " + aDirectory);
        }
    }
}
