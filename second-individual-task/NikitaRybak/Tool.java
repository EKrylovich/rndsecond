package com.rybak.secondTask;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Tool extends SimpleFileVisitor<Path> {
	
	private List<Path> folderPaths = new ArrayList<Path>();
	
    public List<Path> getFolderPaths() {
		return folderPaths;
	}

	@Override
    public FileVisitResult visitFile(Path aFile, BasicFileAttributes aAttrs) throws IOException {
        System.out.println("Processing file:" + aFile);
    	folderPaths.add(aFile);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path aDir, BasicFileAttributes aAttrs) throws IOException {
    	System.out.println("Processing directory:" + aDir);
        return FileVisitResult.CONTINUE;
    }
}