package com.rybak.secondTask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class Test {

	public static void main(String... aArgs) throws IOException{
		
		File file = new File("C:/x/x/");
		File file2 = new File("D:/x/x/");
		
		String firstFolder = "C:/x";
		String secondFolder = "D:/x";
		
		System.out.println(file.mkdirs());
		System.out.println(file2.mkdirs());
		
		new File("C:/x/x/x.txt").createNewFile();
		new File("D:/x/x/x.txt").createNewFile();
		
		FoldersComparator comparator = new FoldersComparator();
		
		List<Path> equalsPaths = comparator.compareFolders(firstFolder, secondFolder, true);
		System.out.println(equalsPaths);
		
  }

}
