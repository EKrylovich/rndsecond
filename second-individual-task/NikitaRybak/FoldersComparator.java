package com.rybak.secondTask;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class FoldersComparator {
	
    public List<Path> compareFolders(String folder1, String folder2, boolean sizeFlag) throws IOException {
    	Tool fileProcessor = new Tool();
		Files.walkFileTree(Paths.get(folder1), fileProcessor);
		Files.walkFileTree(Paths.get(folder2), fileProcessor);

		List<Path> folderPathsList = fileProcessor.getFolderPaths();
		List<Path> equalFiles = new ArrayList<Path>();
		if (sizeFlag) {
			for(int i = 0; i < folderPathsList.size()-1; i++) {
				Path folder01 = folderPathsList.get(i);
				Path folder02 = folderPathsList.get(i+1);
				boolean equalFolders = compareFiles(folderPathsList.get(i), folderPathsList.get(i+1), sizeFlag);
				if(equalFolders) {
					equalFiles.add(folder01);
					equalFiles.add(folder02);
				}
			}
		} else {
			for(int i = 0; i < folderPathsList.size()-1; i++) {
				Path folder01 = folderPathsList.get(i);
				Path folder02 = folderPathsList.get(i+1);
				
				boolean equalFolders = compareFiles(folder01, folder02);
				if(equalFolders) {
					equalFiles.add(folder01);
					equalFiles.add(folder02);
				}
			}
		}
		return equalFiles;
    }
    private boolean compareFiles(Path path1, Path path2) {
    	if(path1.toString().substring(3).equals(path2.toString().substring(3))) {
    		return true;
    	} else {
    		return false;
    	}
    }
    private boolean compareFiles(Path path1, Path path2, boolean sizeFlag) {
    	if(path1.toString().substring(3).equals(path2.toString().substring(3))) {
    		if(path1.toFile().length() == path2.toFile().length()) {
    			return true;
    		}
    		return false;
    	} else {
    		return false;
    	}
    }
}
