package controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.entities.Book;
import model.entities.Magazines;
import model.entities.Stuffs;
import util.logger.CustomLogger;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StuffsLogic {

    public void bookFilter (String inputJsonPath, String outputJsonPath){

        Gson gson = new Gson();
        try {
            List<Stuffs> inputBookList = gson.fromJson(new FileReader(inputJsonPath), new TypeToken<List<Stuffs>>() {
            }.getType());

            List <Book> approvedBookList = new ArrayList<Book>();

            for (Stuffs stuffs : inputBookList) {
                if (stuffs instanceof Book && ((Book) stuffs).getPages() > 100) {
                    approvedBookList.add((Book) stuffs);
                }
                try {
                    FileWriter writer = new FileWriter(outputJsonPath);
                    gson.toJson(approvedBookList, writer);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }catch (IOException ex){
            CustomLogger.errorPrint(ex);
        }
    }

    public void magazineFilter (String inputJsonPath, String outputJsonPath){

        Gson gson = new Gson();
        try {
            List<Stuffs> inputMagazineList = gson.fromJson(new FileReader(inputJsonPath), new TypeToken<List<Stuffs>>() {
            }.getType());

            List <Stuffs> approvedBookList = new ArrayList<Stuffs>();

            for (Stuffs stuffs : inputMagazineList) {
                if (stuffs instanceof Magazines && (!((Magazines) stuffs).isColored())) {
                    approvedBookList.add(stuffs);
                }

                try {
                    FileWriter writer = new FileWriter(outputJsonPath);
                    gson.toJson(approvedBookList, writer);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }catch (IOException ex){
            CustomLogger.errorPrint(ex);
        }
    }



}
