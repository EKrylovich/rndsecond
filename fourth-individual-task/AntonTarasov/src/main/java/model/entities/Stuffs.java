package model.entities;

public class Stuffs {

    protected String name;


    public Stuffs() {
    }

    public Stuffs(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Stuffs{" +
                "name='" + name + '\'' +
                '}';
    }
}
