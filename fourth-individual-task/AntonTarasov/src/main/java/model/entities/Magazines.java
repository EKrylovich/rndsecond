package model.entities;

public class Magazines extends Stuffs {




    private boolean isColored;

    public Magazines() {
        super();
    }

    public Magazines(String name, boolean isColored) {
        super (name);
        this.isColored = isColored;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isColored() {
        return isColored;
    }

    public void setColored(boolean colored) {
        isColored = colored;
    }


}
