package com.fortegroup;

/**
 * Created by ux501 on 28.04.2017.
 */
public class Book extends Asset {

    private int pages;

    public Book(String name, int pages) {
        super(name);
        this.pages = pages;
    }

    public Book() {

    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
