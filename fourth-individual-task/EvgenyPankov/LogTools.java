package com.fortegroup;

/**
 * Created by ux501 on 28.04.2017.
 */
public class LogTools {

    public static void log(final String... pMessages) {
        System.out.println(concat(pMessages));
    }

    public static String concat(final String... pMessages) {
        final StringBuilder result = new StringBuilder();
        for ( final String mes: pMessages ) {
            if ( 0 < result.length() ) {
                result.append(' ');
            }
            result.append(mes);
        }
        return result.toString();
    }

    public static void error(final Throwable pError, final String... pMessages) {
        final String message = concat(pMessages);
        if ( null == pError ) {
            System.out.println(message);
        } else {
            if ( 0 != message.length() ) {
                System.out.println(message);
            }
            pError.printStackTrace();
        }
    }

}
