package com.fortegroup;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ux501 on 28.04.2017.
 */

@JsonAutoDetect
public class BookStore {
    @JsonDeserialize(as=ArrayList.class)
    private List<Asset> assets = new ArrayList<>();

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    public BookStore() {

    }
}
