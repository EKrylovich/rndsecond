package com.fortegroup;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by ux501 on 28.04.2017.
 */
public class JsonParser {
    public static void main(String[] args) throws IOException {

        String json = new Scanner(new File("D:/src.json")).useDelimiter("\\Z").next();


        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        BookStore bookStore = mapper.readValue(json, BookStore.class);




        String jsonTest = mapper.writeValueAsString(bookStore);
        LogTools.log(jsonTest);


    }
}
