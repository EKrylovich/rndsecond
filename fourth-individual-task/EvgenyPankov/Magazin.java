package com.fortegroup;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ux501 on 28.04.2017.
 */
public class Magazin extends Asset {
    @JsonProperty(value="isColor")
    private boolean isColor;

    public Magazin(String name, boolean isColor) {
        super(name);
        this.isColor = isColor;
    }

    public Magazin() {

    }

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean isColor) {
        this.isColor = isColor;
    }
}
