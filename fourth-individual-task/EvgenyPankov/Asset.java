package com.fortegroup;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by ux501 on 28.04.2017.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Book.class, name = "book"),
        @JsonSubTypes.Type(value=Magazin.class, name = "magazin")
})

public class Asset {
    protected String name;

    public Asset(String name) {
        this.name = name;
    }

    public Asset() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
