package task4.oksana;

import com.fasterxml.jackson.core.*;
import task4.oksana.model.Book;
import task4.oksana.model.Magazine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class JSONParser {
    private static final int PAGES = 100;

    public static List<Object> readJson(Reader pFileReader, String pExpr) throws IOException {
        List<Object> objects = new ArrayList<>();
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser = jsonFactory.createParser(pExpr);
        JsonToken jsonToken = jsonParser.nextToken();
        while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jsonParser.getCurrentName();
            if(null == fieldName) {
                continue;
            }
            switch (fieldName) {
                case "type":
                    jsonParser.nextToken();

                    break;
                case "name":
                    jsonParser.nextToken();

                    break;
                case "pages":
                    jsonParser.nextToken();
                default:
                    break;
            }
        }

    }

    public static void writeBookJSON(Writer pFileWriter, List<Book> pBooks) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator jsonGenerator = jsonFactory.createGenerator(pFileWriter);
        for(Book book : pBooks) {
            jsonGenerator.writeFieldName("bigbooks:");
            if(book.getPages() >= PAGES) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("name", book.getName());
                jsonGenerator.writeNumberField("pages", book.getPages());
                jsonGenerator.writeEndObject();
            }
        }
        jsonGenerator.close();
    }

    public static void writeMagazineJSON(Writer pFileWriter, List<Magazine> pMagazines) throws IOException {
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator jsonGenerator = jsonFactory.createGenerator(pFileWriter);
        for(Magazine magazine : pMagazines) {
            jsonGenerator.writeFieldName("colorMagazines:");
            if(magazine.isColor()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("name", magazine.getName());
                jsonGenerator.writeBooleanField("isColor", magazine.isColor());
                jsonGenerator.writeEndObject();
            }
        }
        jsonGenerator.close();
    }
}
