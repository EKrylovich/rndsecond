package task4.oksana.model;

/**
 *
 */
public class Book {
    private String name;
    private int pages;

    public Book(){

    }

    public String getName() {
        return name;
    }

    public int getPages() {
        return pages;
    }

    public void setName(String pName){
        name = pName;
    }

    public void setPages(int pPages) {
        pages = pPages;
    }
}
