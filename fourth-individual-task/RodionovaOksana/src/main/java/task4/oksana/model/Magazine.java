package task4.oksana.model;

/**
 *
 */
public class Magazine {
    private String name;
    private boolean isColor;

    public String getName() {
        return name;
    }

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean pColor) {
        isColor = pColor;
    }

    public void setName(String pName) {
        name = pName;
    }
}
