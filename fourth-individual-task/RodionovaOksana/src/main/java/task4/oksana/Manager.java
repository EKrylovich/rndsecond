package task4.oksana;

import org.omg.CORBA.Object;
import task4.oksana.model.Book;

import java.io.*;
import java.util.List;

/**
 *
 */
public class Manager {
    private static final String SRC_FILE_NAME = "src.json";
    private static final String OUT_BOOKS_FILE_NAME = "dst1.json";
    private static final String OUT_MAGAZINES_FILE_NAME = "dst2.json";
    public Manager(){

    }

    public static void main(String[] args){
        Reader fileReader = null;
        Writer booksFileWriter = null;
        Writer magazinesFileWriter = null;

        try {
            fileReader = new BufferedReader(new FileReader(SRC_FILE_NAME));
            booksFileWriter = new BufferedWriter(new FileWriter(OUT_BOOKS_FILE_NAME));
            magazinesFileWriter = new BufferedWriter(new FileWriter(OUT_MAGAZINES_FILE_NAME));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<Object> books = JSONParser.readJson(fileReader, "{name: \"book\"}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
