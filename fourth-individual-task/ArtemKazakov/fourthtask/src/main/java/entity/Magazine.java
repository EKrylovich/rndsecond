package entity;

public class Magazine {
    private String name;
    private boolean isColor;

    public Magazine(){}

    public Magazine(String name, boolean isColor) {
        this.name = name;
        this.isColor = isColor;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean color) {
        isColor = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Magazine magazine = (Magazine) o;

        if (isColor != magazine.isColor) return false;
        return name != null ? name.equals(magazine.name) : magazine.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (isColor ? 1 : 0);
        return result;
    }
}
