import com.google.gson.*;
import com.google.gson.stream.JsonWriter;
import entity.Book;
import entity.Magazine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JSONParser {

    private static final String BIG_BOOKS_FILENAME = "dst1.json";
    private static final String COLOR_MAGAZINS_FILENAME = "dst2.json";

    public static void parseJson(String path){
        JsonObject jsonObject = new JsonObject();
        List<Book> bigbooks = new ArrayList<Book>();
        List<Magazine> colorMagazins = new ArrayList<Magazine>();

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(path));
            jsonObject = jsonElement.getAsJsonObject();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }

        JsonArray jsonArray = jsonObject.getAsJsonArray("assets");

        for(JsonElement jsonElement : jsonArray){
            JsonObject obj = jsonElement.getAsJsonObject();
            if (obj.get("type").getAsString().equals("book")){
                if (obj.get("pages").getAsInt() >= 100) {
                    bigbooks.add(new Book(obj.get("name").getAsString(), obj.get("pages").getAsInt()));
                }
            }
            if (obj.get("type").getAsString().equals("magazin")){
                if ( obj.get("isColor").getAsBoolean()) {
                    colorMagazins.add(new Magazine(obj.get("name").getAsString(), obj.get("isColor").getAsBoolean()));
                }
            }
        }


        Gson gson = new Gson();
        String bigBooksJson = gson.toJson(bigbooks);

        try {
            File myFile = new File(BIG_BOOKS_FILENAME);
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(bigBooksJson);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e){
            System.out.println("Ошибка записи в файл");
        }

        String colorMagazines = gson.toJson(colorMagazins);
        try {
            File colorMagazinesFile = new File(COLOR_MAGAZINS_FILENAME);
            colorMagazinesFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(colorMagazinesFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(colorMagazines);
            myOutWriter.close();
            fOut.close();
        } catch (Exception e){
            System.out.println("Ошибка записи в файл");
        }
    }

}
