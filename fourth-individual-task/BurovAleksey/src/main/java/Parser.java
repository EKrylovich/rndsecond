import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Book;
import models.Library;
import models.Literature;
import models.Magazine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple paser with one method to do parsing and filtering of the library
 * @author Alexey Burov
 * @version 1.0
 */
public class Parser {
    public static void doParse(){
        try(BufferedReader reader =  new BufferedReader(new InputStreamReader(new FileInputStream("src.json")));
            BufferedWriter dist1 = new BufferedWriter(new FileWriter("dist1.json"));
            BufferedWriter dist2 = new BufferedWriter(new FileWriter("dist2.json"))) {

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES,true);
            Library library = null;
            try {
                library = mapper.readValue(reader, Library.class);
            } catch (IOException e) {
                Logger.log(e.toString());
            }
            List<Book> books = new ArrayList<>();
            List<Magazine> magazins = new ArrayList<>();
            for (Literature literature : library.assets) {
                if (literature instanceof Book) {
                    Book book = (Book) literature;
                    if (book.pages > 100)
                        books.add(book);
                } else if (literature instanceof Magazine) {
                    Magazine magazine = (Magazine) literature;
                    if (magazine.isColor)
                        magazins.add(magazine);
                }
            }

            mapper.writerWithDefaultPrettyPrinter().writeValue(dist1, books);
            mapper.writerWithDefaultPrettyPrinter().writeValue(dist2, magazins);
        }catch (IOException e){
            Logger.log(e.toString());
        }
    }
}
