package models;

/**
 * Created by alex on 4/28/17.
 */

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Simple model

 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Book.class, name = "book"),
        @JsonSubTypes.Type(value = Magazine.class, name = "magazin")
})
@JsonAutoDetect
public class Literature {
    public String name;

    @Override
    public String toString() {
        return "Literature{"+name+"}";
    }
}
