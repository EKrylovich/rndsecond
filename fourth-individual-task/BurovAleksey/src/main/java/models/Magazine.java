package models;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by alex on 4/28/17.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "className")
@JsonSubTypes(@JsonSubTypes.Type( value = Magazine.class,name = "magazin"))
public class Magazine extends Literature {
    public boolean isColor;

    @Override
    public String toString() {
        return "Magazine{name="+name +
                ",isColor=" + isColor +
                '}';
    }
}
