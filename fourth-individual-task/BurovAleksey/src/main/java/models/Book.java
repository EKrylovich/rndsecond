package models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Simple model
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "className")
@JsonSubTypes(@JsonSubTypes.Type( value = Book.class,name = "book"))
@JsonAutoDetect
public class Book extends Literature {
    public int pages;

    public Book() {
    }


    @Override
    public String toString() {
        return "Book{name=" +name+
                ",pages=" + pages +
                '}';
    }
}
