package models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;

/**
 * Simple model
 */
@JsonAutoDetect
public class Library {
    public Literature[] assets;
}
