package task4;


import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class FileHandler {

    public static void writeScr(List<Entity> list) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new FileOutputStream(
                    System.getProperty("user.dir") + File.separator
                            + "scr.json"), list);
        } catch (IOException ex) {

        }
    }

    public static List<Entity> read() {
        ObjectMapper mapper = new ObjectMapper();
        String filepath = System.getProperty("user.dir") + File.separator
                + "scr.json";
        List<Entity> list = null;
        try {
            list = mapper.readValue(new FileInputStream(filepath),
                    ArrayList.class);
        } catch (IOException ex) {

        }
        return list;
    }
}