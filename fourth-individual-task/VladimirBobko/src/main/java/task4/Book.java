package task4;

/**
 * Created by TimeLine on 28.04.2017.
 */
public class Book extends Entity {

    private String name;
    private int page;

    public Book() {
    }

    public Book(String name, int page) {
        this.name = name;
        this.page = page;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
