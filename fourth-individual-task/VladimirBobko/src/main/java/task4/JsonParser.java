package task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonParser {
    public static List<Entity> jsonPars(List tableJson) {
        List<Entity> list = new ArrayList<>();
        for (Object o : tableJson) {
            Map map = (Map) o;
            Entity entity = new Entity();
            entity.setType((String) map.get("type"));
            if (entity.getType().equals("book")) {
                entity = (Book) entity;
                ((Book) entity).setName((String) map.get("name"));
                ((Book) entity).setPage((Integer) map.get("page"));
                if (((Book) entity).getPage() > 100) {
                    list.add(entity);
                }
            } else if (entity.getType().equals("magazin")) {
                entity = (Magazin) entity;
                ((Magazin) entity).setName((String) map.get("name"));
                ((Magazin) entity).setColor((Boolean) map.get("isColor"));
                if (((Magazin) entity).isColor())
                    list.add(entity);
            }
        }

        return list;
    }
}