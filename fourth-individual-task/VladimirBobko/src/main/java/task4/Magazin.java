package task4;


/**
 * Created by TimeLine on 28.04.2017.
 */
public class Magazin extends Entity {

    private boolean isColor;
    private String name;

    public boolean isColor() {
        return isColor;
    }

    public void setColor(boolean color) {
        isColor = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
