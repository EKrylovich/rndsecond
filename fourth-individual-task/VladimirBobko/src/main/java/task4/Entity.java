package task4;

/**
 * Created by TimeLine on 28.04.2017.
 */
public class Entity {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
