package com.company;

/**
 * Created by PC on 28.04.2017.
 */
public class Asset {

    private String type;

    private String name;

    private long pages;

    private boolean isColor;

    public Asset() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPages() {
        return pages;
    }

    public void setPages(long pages) {
        this.pages = pages;
    }

    public boolean isIsColor() {
        return isColor;
    }

    public void setIsColor(boolean isColor) {
        this.isColor = isColor;
    }

    public String toString(boolean flag) {
        if(flag) {
            return "Asset{" +
                    "type='" + type + '\'' +
                    ", name='" + name + '\'' +
                    ", isColor=" + isColor +
                    '}';
        }
         return "Asset{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", pages=" + pages +
                '}';
    }
}
