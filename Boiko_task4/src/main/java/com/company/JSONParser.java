package com.company;

import com.google.gson.stream.JsonReader;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 28.04.2017.
 */
public class JSONParser {

    public JSONParser() {
    }

    public void parseJsonFile(String path) throws IOException {
        JsonReader jsonReader = new JsonReader(new FileReader(path));

        jsonReader.beginObject();
        List<Asset> list = new ArrayList<>();
        String nameMethod = "";
        while (jsonReader.hasNext()) {

            String name = jsonReader.nextName();
            nameMethod = name;
            if (name.equals("assets")) {
                readApp(jsonReader, list);

            }
            if (name.equals("bigbooks")) {
                readApp(jsonReader, list);

            }
        }

        jsonReader.endObject();
        jsonReader.close();

        if (nameMethod.equals("bigbooks")) {
            File json = new File("output1.json");
            for (Asset asset : list) {
                if(asset.getPages() < 100){
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.writeValue(json, asset);
                }
            }
        }
        if (nameMethod.equals("assets")) {
            boolean flag;
            for (Asset asset : list) {
                if (asset.getType().equals("magazin")) {
                    flag = true;
                    System.out.println(asset.toString(flag));
                } else {
                    flag = false;
                    System.out.println(asset.toString(flag));
                }
            }
        }

    }

    public List readApp(JsonReader jsonReader, List list) throws IOException {
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            Asset asset = new Asset();
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();
                if (name.equals("type")) {
                    //System.out.println(jsonReader.nextString());
                    asset.setType(jsonReader.nextString());
                }
                if (name.equals("name")) {
                    // System.out.println(jsonReader.nextString());
                    asset.setName(jsonReader.nextString());
                }
                if (name.equals("pages")) {
                    //System.out.println(jsonReader.nextInt());
                    asset.setPages(jsonReader.nextInt());
                }
                if (name.equals("isColor")) {
                    // System.out.println(jsonReader.nextBoolean());
                    asset.setIsColor(jsonReader.nextBoolean());
                }
            }
            jsonReader.endObject();
            list.add(asset);
        }
        jsonReader.endArray();
        return list;
    }

}
