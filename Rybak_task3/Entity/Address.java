package com.rybak._3.Entity;

public class Address {
	private int buildingNumber;
	private Street street;
	
	public Address(int buildingNumber, Street street) {
		super();
		this.buildingNumber = buildingNumber;
		this.street = street;
	}

	public int getBuildingNumber() {
		return buildingNumber;
	}

	public Street getStreet() {
		return street;
	}

	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public void setStreet(Street street) {
		this.street = street;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + buildingNumber;
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (buildingNumber != other.buildingNumber)
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}
	
	
	
	
}
