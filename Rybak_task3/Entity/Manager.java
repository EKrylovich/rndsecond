package com.rybak._3.Entity;

import java.util.concurrent.ConcurrentSkipListSet;

public class Manager {
	private ConcurrentSkipListSet<Building> buildings;
	private ConcurrentSkipListSet<Street> streets;
	
	public Manager(ConcurrentSkipListSet<Building> buildings, ConcurrentSkipListSet<Street> streets) {
		super();
		this.buildings = buildings;
		this.streets = streets;
	}

	public synchronized Building getBuildingByAddress(Address address){
		for(Building building : buildings) {
			if(building.contains(address)) {
				return building;
			}
		}
		return null;
	}
	public synchronized void deleteBuildingByAddress(Address address){
		for(Building building : buildings) {
			if(building.contains(address)) {
				buildings.remove(building);
			}
		}
	}
	public synchronized void addBuildings(Building building){
		buildings.add(building);
	}
	public synchronized Street getStreetByName(String name){
		for(Street street : streets) {
			if(street.getName().equals(name)) {
				return street;
			}
		}
		return null;
	}
	public synchronized void deleteStreetByName(String name){
		for(Street street : streets) {
			if(street.getName().equals(name)) {
				streets.remove(street);
			}
		}
	}
	public synchronized void addStreet(Street street){
		streets.add(street);
	}
	public synchronized void getStreetByStreesNamePart(String name){
		for(Street street : streets) {
			if(street.getName().contains(name)) {
				streets.remove(street);
			}
		}
	}
	public synchronized Building getBuildingByByStreesNamePartAndRange(String streersNamepart, int fromrange, int toRange){
		for(Building building : buildings) {
			
		}
		return null;
	}
}
