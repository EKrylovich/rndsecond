package com.rybak._3.Entity;

import java.util.concurrent.ConcurrentSkipListSet;

public class Building {

	private ConcurrentSkipListSet<Address> addresses;

	public Building(ConcurrentSkipListSet<Address> addresses) {
		super();
		this.addresses = addresses;
	}
	
	public Building() {
		super();
		this.addresses = new ConcurrentSkipListSet<>();
	}
	
	public ConcurrentSkipListSet<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(ConcurrentSkipListSet<Address> addresses) {
		this.addresses = addresses;
	}

	public synchronized boolean contains(Address address){
		return addresses.contains(address);
	}
	
	
}
