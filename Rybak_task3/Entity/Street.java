package com.rybak._3.Entity;

import java.util.concurrent.ConcurrentSkipListSet;

public class Street {
	
	private String name;
	private ConcurrentSkipListSet<Building> buildings;

	public Street(String name, ConcurrentSkipListSet<Building> buildings) {
		super();
		this.name = name;
		this.buildings = buildings;
	}
	
	public Street(String name) {
		this.name = name;
		this.buildings = new ConcurrentSkipListSet<Building>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Street other = (Street) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Street [name=" + name + ", buildings=" + buildings + "]";
	}
	
}
