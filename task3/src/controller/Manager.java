package controller;

import model.entity.Address;
import model.entity.Building;
import model.entity.Street;
import model.util.creator.RandomCreator;
import model.util.logger.CustomLogger;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Manager  {

    private static Manager instance;

    public static synchronized Manager getInstance(){

        if (instance == null) {
            instance = new Manager();
        }
        return instance;
    }

    private Manager() {
        super();
    }


    private Set<Building> buildingSet = Collections.synchronizedSet(new HashSet<>());
    private Set<Street> streetSet = Collections.synchronizedSet(new HashSet<>());
    private Set<Address> addresses = Collections.synchronizedSet(new HashSet<>());



    public synchronized Building createNewBuilding(int StreetCount){

        Building building = new Building();

        for (int i = 0; i < StreetCount; i++) {

            Street street = new Street(RandomCreator.randomStreetName());
            if (!streetSet.contains(street)){
                streetSet.add(street);
            }
            Address address = new Address(building.getBuildingId(),street);
            addresses.add(address);
            building.addAddress(street,building.getBuildingId());

        }

        buildingSet.add(building);
        return building;
    }



    public synchronized void removeStreet (Street street){

        if (streetSet.contains(street)){
            streetSet.remove(street);
            CustomLogger.log("This street has been removed" + street);
        }

    }

    public boolean isContainStreet (Street stringName){

        return  streetSet.contains(stringName);

    }


}
