package model.util.logger;

/*
Class which provides  logging functions.
 */
public class CustomLogger {

    /*
    This method is used for logging any text information.
     */
    public static void log(String... messages) {
        System.out.println(concat(messages));
    }

    public static String concat(String... messages) {
        StringBuilder sb = new StringBuilder();
        for (String word : messages) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(word);
        }
        return sb.toString();
    }

    /*
    This method is used for logging information about exclusive situation which occurred
     during the program execution.
     */
    public static void errorPrint(Throwable exception, String... messages) {
        String message = concat(messages);
        if (exception == null) {
            System.out.println(message);
        } else{
            if (message.length() > 0) {
                System.out.println(message);
            }
            exception.printStackTrace();
        }
    }
}
