package model.util.creator;



public class RandomCreator {


        static String streetCreator [] = {"First","Second","Third","Fourth"};



        static String lastName [] = {"Black","Smith","Fox","White"};
        static String company[] = {"Bosh","Samsung","Apple","MTZ"};
        static String employeeTitle [] = {"HR","Manager","Director"};

        public static int randomSsn(int range){
            return (int)(Math.random()*1000);
        }

        public static String randomStreetName(){

            return streetCreator[(int)(Math.random()* streetCreator.length)];
        }

        public static String randomLastName (){
            return lastName[(int)(Math.random()*lastName.length)];
        }
        public static String randomCompanyTitle (){
            return company[(int)(Math.random()*company.length)];
        }
        public static String randomEmployeeTitle (){
            return employeeTitle[(int)(Math.random()*employeeTitle.length)];
        }


}
