package model.entity;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class Building {


   private static int count;
   private Integer buildingId = count++;
   private CopyOnWriteArrayList <Address> addresses = new CopyOnWriteArrayList<>();

    public Building() {
    }

    public Building(Integer buildingId, CopyOnWriteArrayList<Address> addresses) {
        this.buildingId = buildingId;
        this.addresses = addresses;
    }

    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }

    public CopyOnWriteArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(CopyOnWriteArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public synchronized void addAddress (Street street, int number){

        addresses.add(new Address(number,street));
    }
}
