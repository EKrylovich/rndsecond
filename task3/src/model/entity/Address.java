package model.entity;

public class Address {


    private int buildingNumber;
    private Street streetTitle;

    public Address() {

    }

    public Address(int buildingNumber, Street streetTitle) {
        this.buildingNumber = buildingNumber;
        this.streetTitle = streetTitle;
    }


    public int getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(int buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Street getStreetTitle() {
        return streetTitle;
    }

    public void setStreetTitle(Street streetTitle) {
        this.streetTitle = streetTitle;
    }


}
