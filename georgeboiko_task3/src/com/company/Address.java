package com.company;

/**
 * Created by PC on 19.04.2017.
 */
public class Address {
    private int idStreet;
    private int idBuilding;

    public Address(int idStreet, int idBuilding) {
        this.idStreet = idStreet;
        this.idBuilding = idBuilding;
    }


    public int getIdStreet() {
        return idStreet;
    }

    public void setIdStreet(int idStreet) {
        this.idStreet = idStreet;
    }

    public int getIdBuilding() {
        return idBuilding;
    }

    public void setIdBuilding(int idBuilding) {
        this.idBuilding = idBuilding;
    }

}
