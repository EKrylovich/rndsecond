package com.company;

import sun.plugin2.message.SetAppletSizeMessage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by PC on 19.04.2017.
 */
public class Manager implements Runnable{
    private Map<Street,Set<Address>> mapBuiling = new HashMap<>();
    private Map<Building,Set<Address>> mapStreet = new HashMap<>();
    private Integer idStreet = 0;
    private Integer idBuilding = 0;

    public Manager(){}

    public synchronized void addBuiling(String addressBuiling,int numberBuilding){
        Set<Address>setAddress = new HashSet<>();
        boolean flag = false;
        if(mapStreet.isEmpty()){
            idBuilding++;
        }
        if(mapBuiling.isEmpty()){
            idStreet++;
            flag = true;
        }
        if(!flag) {
            for (Map.Entry<Building, Set<Address>> entry : mapStreet.entrySet()) {
                Set<Address> entryA = entry.getValue();
                for (Address address : entryA) {
                    if (!address.equals(addressBuiling)) {
                        idBuilding++;
                        idStreet++;
                        Street street = new Street(idStreet, addressBuiling);
                        Building building = new Building(idBuilding,numberBuilding);
                        Address address1 = new Address(idBuilding, idStreet);
                        setAddress.add(address1);
                        mapBuiling.put(street, setAddress);
                        mapStreet.put(building, setAddress);
                    }
                }
            }
        }else{
            Street street = new Street(idStreet,addressBuiling);
            Building building = new Building(idBuilding,numberBuilding);
            Address address1 = new Address(idBuilding,idStreet);
            setAddress.add(address1);
            mapBuiling.put(street,setAddress);
            mapStreet.put(building,setAddress);
        }
    }

    public synchronized void updateBuildingNumber(String nameString,int number){
        for(Map.Entry<Street,Set<Address>> entry:mapBuiling.entrySet()){
            Set<Address>addresse = entry.getValue();
            if (entry.getKey().getName().contains(nameString)) {
                for(Address address:addresse) {
                    for(Map.Entry<Building,Set<Address>> build:mapStreet.entrySet()){
                        if(address.getIdBuilding() == build.getKey().getId()){
                            build.getKey().setNumber(number);
                        }
                    }
                }
            }
        }
    }

    public synchronized void getBuilingByPartName(String name){
        for(Map.Entry<Street,Set<Address>> entry:mapBuiling.entrySet()){
            Set<Address>addresse = entry.getValue();
            if (entry.getKey().getName().contains(name)) {
                 for(Address address:addresse) {
                     for(Map.Entry<Building,Set<Address>> build:mapStreet.entrySet()){
                         if(address.getIdBuilding() == build.getKey().getId()){
                             System.out.println(entry.getKey().getName() + " " + build.getKey().getNumber());
                         }
                     }
                }
            }
        }
    }

    @Override
    public void run() {
        addBuiling("nezavisimosti",39);
        addBuiling("peramogi ",83);
        getBuilingByPartName("per");
        updateBuildingNumber("nezavisimosti",49);
        getBuilingByPartName("nez");
    }
}
