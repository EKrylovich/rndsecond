/**
 * @author Mikhail Kovalev
 */
public class Account {
	private volatile int amount;
	private volatile int doneWorkPerson;

	public Account(final int amount){
		this.amount = amount;
		doneWorkPerson = 0;
	}

	public synchronized void increaseAmount(final int increaseAmount){
		amount += increaseAmount;
	}

	public synchronized void decreaseAmount(final int decreaseAmount){
		if (amount - decreaseAmount >= 0){
			amount -= decreaseAmount;
		}
	}

	public int getAmount(){
		return amount;
	}

	public int getDoneWorkPerson() {
		return doneWorkPerson;
	}

	public synchronized int increaseDoneWorkPerson(){
		return ++doneWorkPerson;
	}
}
