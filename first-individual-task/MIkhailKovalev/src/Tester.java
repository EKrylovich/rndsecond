import java.util.Random;

/**
 * @author Mikhail Kovalev
 */
public class Tester {
	public static void main(String[] args) {
		Account account = new Account(new Random().nextInt(16));
		Person person1 = new Person(account, "Petr");
		Person person2 = new Person(account, "Ivan");
		Person person3 = new Person(account, "Vasya");

		new Thread(() -> person1.doTasks()).start();
		new Thread(() -> person2.doTasks()).start();
		new Thread(() -> person3.doTasks()).start();
	}
}
