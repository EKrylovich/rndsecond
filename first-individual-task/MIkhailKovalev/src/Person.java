import java.util.Random;

/**
 * @author Mikhail Kovalev
 */
public class Person extends Thread {
	private Account account;
	private String name;
	private volatile int taskDoneNumber;

	public Person(final Account account, final String name){
		this.account = account;
		this.name = name;
	}

	public void doTasks() {
		int initialBalans = account.getAmount();
		int finishBalans;

		System.out.println("Start work person: " + name);
		taskDoneNumber = 0;
		new Thread(() -> {
			Random sleepRandom = new Random();
			Random increaseRandom = new Random();

			for (int i = 0; i < 50; i++) {
				try {
					Thread.sleep(sleepRandom.nextInt(4) + 2);
					account.increaseAmount(increaseRandom.nextInt(3) + 1);
					//TODO: save history
				} catch (InterruptedException pE) {
					pE.printStackTrace();
				}
			}
			taskDoneNumber++;
		}).start();

		new Thread(() -> {
			Random sleepRandom = new Random();
			Random increaseRandom = new Random();

			for (int i = 0; i < 30; i++) {
				try {
					Thread.sleep(sleepRandom.nextInt(3) + 2);
					account.increaseAmount(increaseRandom.nextInt(4) + 3);
					//TODO: save history
				} catch (InterruptedException pE) {
					pE.printStackTrace();
				}
			}
			taskDoneNumber++;
		}).start();

		new Thread(() -> {
			Random decreaseRandom = new Random();
			while (taskDoneNumber < 2){
				if (account.getAmount() > 10) {
					account.decreaseAmount(decreaseRandom.nextInt(3) + 8);
					//TODO: save history
				}
			}
		}).start();

		while (taskDoneNumber < 2){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException pE) {
				pE.printStackTrace();
			}
		}
		finishBalans = account.getAmount();

		if (account.increaseDoneWorkPerson() < 3){
			System.out.println("Finish work person: " + name);
			System.out.println(initialBalans);
			//TODO: write history
			System.out.println(finishBalans);
		} else {
			System.out.println("Person: " + name + "ne uspel");
		}
	}

}
