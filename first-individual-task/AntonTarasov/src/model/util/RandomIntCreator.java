package model.util;

import java.util.Random;

public class RandomIntCreator {

    private static Random random = new Random();

    public static int randomInt(int range){

        return random.nextInt(range);

    }

    public static int randomIntWithRange (int from, int to){

        return random.nextInt((to-from) + from );

    }


}
