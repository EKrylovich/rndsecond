package model.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Clock{

    private static SimpleDateFormat df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss:SS");
    public static String getActualTime(){

        return df.format(Calendar.getInstance().getTime());
    }

}
