package model.entity;

import model.util.RandomIntCreator;

import java.util.concurrent.TimeUnit;

public class Husband implements Runnable {

    private Account account;
    private int lowRangeAmount;
    private int highRangeAmount;
    private int lowRangeTime;
    private int highRangeTime;
    private int opperationTimes;

    public Husband(Account account, int lowRangeAmount, int highRangeAmount, int lowRangeTime, int highRangeTime) {
        this.account = account;
        this.lowRangeAmount = lowRangeAmount;
        this.highRangeAmount = highRangeAmount;
        this.lowRangeTime = lowRangeTime;
        this.highRangeTime = highRangeTime;
    }

    public Husband(Account account) {
        this.account = account;
    }

    @Override
    public void run() {

        while (!Thread.interrupted()){

                for (int i = 0; i < opperationTimes; i++){
                    try {
                        account.add(RandomIntCreator.randomIntWithRange(lowRangeAmount,highRangeAmount));
                        TimeUnit.SECONDS.wait(RandomIntCreator.randomIntWithRange(lowRangeTime,highRangeTime));
                    }catch (InterruptedException ex){
                        ex.printStackTrace();
                    }
                }
        }
    }
}
