package model.entity;

import model.util.Logger;
import model.util.RandomIntCreator;

public class Account {

    private int balance;
    private final static int INITIAL_AMOUNT = 15;
    public static int operationID;
    private static final int MIN_AMOUNT = 10;
    private OperationStatus operationStatus;
    private OperationType operationType;
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public synchronized int  getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Account() {
        balance = RandomIntCreator.randomInt(INITIAL_AMOUNT);
    }

    public synchronized void add (int value){


        setOperationType(OperationType.ADDING);
        operationID++;
        balance+=value;
        setOperationStatus(OperationStatus.OK);
        Logger.accountLog(this);
        notifyAll();
    }


    public synchronized void withdraw (int value){
        setOperationType(OperationType.WITHDRAW);
        operationID++;
        if (balance > MIN_AMOUNT){

            balance -= value;
            setOperationStatus(OperationStatus.OK);
            Logger.accountLog(this);

        }
        else {
            setOperationStatus(OperationStatus.REJECTED);
            Logger.accountLog(this);
            this.notifyAll();
        }
    }

}
