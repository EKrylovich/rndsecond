package model.entity;

import model.util.RandomIntCreator;

public class Wife implements Runnable {

    private Account account;
    private int lowRangeValue;
    private int highRangeValue;

    public Wife(Account account, int lowRangeValue, int highRangeValue) {
        this.account = account;
        this.lowRangeValue = lowRangeValue;
        this.highRangeValue = highRangeValue;
    }

    @Override
    public void run() {

            while (!Thread.interrupted()){

                account.withdraw(RandomIntCreator.randomIntWithRange(8,10));

            }


    }
}
