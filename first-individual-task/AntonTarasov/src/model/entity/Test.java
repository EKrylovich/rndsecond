package model.entity;

import java.util.Random;

public class Test {

    public static void main(String[] args) {

        Account account = new Account();
        Husband husband1 = new Husband(account,2,5,1,3);
        Husband husband2 = new Husband(account,3,6,2,4);
        Wife wife = new Wife(account,8,10);

        Thread firstThread = new Thread(husband1);
        Thread secondThread = new Thread(husband2);
        Thread thirdThread = new Thread(wife);
        firstThread.start();
        secondThread.start();
        thirdThread.start();

    }
}
