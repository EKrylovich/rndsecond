package by.fortegroup.eCommerceLab.task1;

import static java.lang.Thread.sleep;

/**
 * Created by akloster on 4/5/2017.
 */
public class Getter extends Thread{
    private Account account;

    public Getter(Account account) {
        this.account = account;
    }

    public void run() {
        for(int i=0; i<50; i++) {
            account.get();
            try {

                sleep(Account.getRandInt(1000, 3000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
