package by.fortegroup.eCommerceLab.task1;

import java.util.Random;

/**
 * Created by akloster on 4/5/2017.
 */
public class Account {
    private Integer accountAmount  = new Integer(getRandInt(0,15));

    public synchronized void add (int addValue) {
        accountAmount += addValue;
        notifyAll();
        System.out.println("Put, now: " + accountAmount);
    }

    public Integer getAccountAmount() {
        return accountAmount;
    }

    public synchronized void get () {
        while(accountAmount<10) {
            try {

                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        accountAmount-=getRandInt(8, 10);
        System.out.println("GET, now: " + accountAmount);
        notifyAll();
    }
    public static int getRandInt(int min, int max) {
        return (int) Math.random()*(max-min)+min;
    }
}
