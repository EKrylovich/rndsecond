package by.fortegroup.eCommerceLab.task1;

public class Main {

    public static void main(String[] args) {
	    Account account = new Account();
	    Adder adder1 = new Adder(account, new Person(50, 2, 5, 1000, 3000));
        Adder adder2 = new Adder(account, new Person(30, 3, 6, 2000, 4000));
        Getter getter = new Getter(account);
        adder1.start();
        adder2.start();
        getter.start();
        try {
        adder1.join();
            adder2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("log:");
        System.out.println(adder1.log);
        System.out.println(adder2.log);
        System.out.println("Final amount:" + account.getAccountAmount());
    }
}
