package by.fortegroup.eCommerceLab.task1;

/**
 * Created by akloster on 4/5/2017.
 */
public class Person {


    public Person(int add, int minAdd, int maxAdd, int minSleep, int maxSleep) {
        this.add = add;
        this.minAdd = minAdd;
        this.maxAdd = maxAdd;
        this.minSleep = minSleep;
        this.maxSleep = maxSleep;
    }
    public int add;
    public int minAdd;
    public int maxAdd;
    public int minSleep;
    public int maxSleep;
}
