package by.fortegroup.eCommerceLab.task1;

import java.sql.Time;

/**
 * Created by akloster on 4/5/2017.
 */
public class Log {
    private int add;
    private Person person;
    private long time;

    public int getAdd() {
        return add;
    }

    public void setAdd(int add) {
        this.add = add;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Log(int add, Person person, long time) {

        this.add = add;
        this.person = person;
        this.time = time;
    }
}
