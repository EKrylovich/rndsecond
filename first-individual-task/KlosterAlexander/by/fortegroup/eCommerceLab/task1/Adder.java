package by.fortegroup.eCommerceLab.task1;

import java.util.ArrayList;
import java.sql.Date;

/**
 * Created by akloster on 4/5/2017.
 */
public class Adder extends Thread {
    private Account account;
    private Person personInit;
    public ArrayList<Log> log = new ArrayList<>();

    public Adder(Account account, Person personInit) {
        this.account = account;
        this.personInit = personInit;
    }

    public void run() {
        for(int i=0; i<personInit.add; i++) {
            int add = Account.getRandInt(personInit.minAdd,personInit.maxAdd);
            account.add(add);
            log.add(new Log(add, personInit, System.currentTimeMillis()));
            try {
                sleep(Account.getRandInt(personInit.minSleep, personInit.maxSleep));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
