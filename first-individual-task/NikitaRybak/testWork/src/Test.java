import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by pavel on 5.4.17.
 */
public class Test {

    public static void main(String[] args) throws InterruptedException {
        Account account = new Account();
        ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(100);
        Person person1 = new Person(queue, account, 50, 2, 5, 1, 3);
        Person person2 = new Person(queue, account, 30, 3, 6, 2, 4);

        Thread t1 = new Thread(person1);
        t1.start();
        Thread t2 = new Thread(person2);
        t2.start();

        PersonConsumer person3 = new PersonConsumer(queue, account);
        Thread t3 = new Thread(person3);
        t3.start();

        t1.join();
        t2.join();

        for (String s : queue) {
            System.out.println(s);
        }


    }

}
class Person implements Runnable {

    private Account account;
    private ArrayBlockingQueue<String> queue;
    private int countOfTimes;
    private int fromAmount;
    private int toAmount;
    private int fromTimes;
    private int toTimes;


    Person(ArrayBlockingQueue<String> queue, Account account, int countOfTimes, int fromAmount, int toAmount, int fromTimes, int toTimes) {
        this.account = account;
        this.queue = queue;
        this.countOfTimes = countOfTimes;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.fromTimes = fromTimes;
    }

    @Override
    public void run() {
        synchronized(account) {
            for (int i = 0; i < countOfTimes; i++) {
                int amount = RandomGenerator.getRandom(fromAmount, toAmount);
                account.addToBalance(amount);
                queue.add("Add " + amount + account.getBalance());
                account.notifyAll();
                try {
                    Thread.sleep(RandomGenerator.getRandom(1, 3)*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
class PersonConsumer implements Runnable {

    private Account account;
    private ArrayBlockingQueue<String> queue;


    PersonConsumer(ArrayBlockingQueue<String> queue, Account account) {
        this.account = account;
        this.queue = queue;

    }

    @Override
    public void run() {
        synchronized(account) {
            while(account.getBalance() <= 10) {
                try {
                    account.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int quantity = RandomGenerator.getRandom(8, 10);
            int amount = (int) account.getFromBalance(quantity);
            queue.add("Removing " + quantity + " " + amount);
        }

    }

}