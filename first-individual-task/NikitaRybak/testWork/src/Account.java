/**
 * Created by pavel on 5.4.17.
 */
public class Account {

    private volatile long balance;

    Account() {
        this.balance = System.currentTimeMillis()%16;
    }

    public synchronized void addToBalance(int amount) {
        balance += amount;
    }
    public synchronized long getFromBalance(int amount) {
        balance = balance - amount;
        return  balance;
    }
    public synchronized long getBalance() {
        return  balance;
    }
}
