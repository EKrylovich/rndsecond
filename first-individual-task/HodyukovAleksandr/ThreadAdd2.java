package com.task1;

import java.util.concurrent.TimeUnit;

public class ThreadAdd2 extends Thread{
	private PersonAdd person;
	
	public ThreadAdd2(PersonAdd person) {
		this.person = person;
	}
	@Override
	public void run() {
		for ( int i = 0; i < 30; i++ ) {
			int time = (int) ((Math.random() * 3) + 2);
			try {
				TimeUnit.SECONDS.sleep(time);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int amount = (int) ((Math.random() * 4) + 3);
			person.deposit(amount);
		}
	
	}
}
