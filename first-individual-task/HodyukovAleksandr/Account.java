package com.task1;

public class Account {
	private long balance;
	
	public Account(long balance) {
		this.balance = balance;
	}
	
	public void addBalance(int amount) {
		balance += amount;
	}
	
	public void credit(int amount) {
		balance -= amount;
	}
	
	public long getBalance() {
		return balance;
	}
}
