package com.task1;

public class PersonAdd {
	private Account account;
	
	public PersonAdd(Account account) {
		this.account = account;
	}
	
	public void deposit(int deposit) {
		account.addBalance(deposit);
	}
}
