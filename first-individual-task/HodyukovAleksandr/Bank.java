package com.task1;

public class Bank {

	public static void main(String[] args) {
		Account account = new Account((long)(Math.random()*16));
		
		PersonAdd pAdd1 = new PersonAdd(account);
		PersonAdd pAdd2 = new PersonAdd(account);
		PersonGet pGet = new PersonGet(account);
		
		ThreadAdd1 threadAdd1 = new ThreadAdd1(pAdd1);
		threadAdd1.start();
		ThreadAdd2 threadAdd2 = new ThreadAdd2(pAdd2);
		threadAdd2.start();
		ThreadGet threadGet = new ThreadGet(pGet);
		threadGet.start();

	}

}
