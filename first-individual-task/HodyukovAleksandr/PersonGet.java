package com.task1;

public class PersonGet {
private Account account;
	
	public PersonGet(Account account) {
		this.account = account;
	}
	
	public void withdraw() {
		synchronized (account) {
			while ( account.getBalance() <= 10 ) {
				try {
					account.wait();
				} catch (InterruptedException e) {
					
				}
			}
			int amount = (int) ((Math.random() * 9) + 2);
			account.credit(amount);
		}
		
	}
	
	
}
