package com.task1;

import java.util.concurrent.TimeUnit;

public class ThreadAdd1 extends Thread{
	private PersonAdd person;
	
	public ThreadAdd1(PersonAdd person) {
		this.person = person;
	}
	@Override
	public void run() {
		for ( int i = 0; i < 50; i++ ) {
			int time = (int) ((Math.random() * 2) + 2);
			try {
				TimeUnit.SECONDS.sleep(time);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int amount = (int) ((Math.random() * 3) + 3);
			person.deposit(amount);
			
		}
	
	}
}
