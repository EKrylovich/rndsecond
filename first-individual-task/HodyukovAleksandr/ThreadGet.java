package com.task1;

public class ThreadGet extends Thread {
	private PersonGet person;
	
	public ThreadGet(PersonGet person) {
		this.person = person;
	}
	@Override
	public void run() {
		person.withdraw();
	}
}
