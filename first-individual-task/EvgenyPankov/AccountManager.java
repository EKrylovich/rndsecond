import java.util.Map;

/**
 * Created by forte on 5.4.17.
 */
public class AccountManager {
    public static void main(String[] args) {
        Account account = new Account();
        Producer person1 = new Producer(account, 5, 2, 3, 1, 50);
        Producer person2 = new Producer(account, 6, 3, 4, 2, 30);
        Reveiver person3 = new Reveiver(8, 10);

        Thread thread1 = new Thread(person1);
        Thread thread2 = new Thread(person2);
        Thread thread3 = new Thread(person3);

        thread1.start();
        thread2.start();
        thread3.start();

        while (true) {
            if (!thread1.isAlive() && !thread2.isAlive()) {
                System.out.println(account.getInitialAmount());


                for (Map.Entry<String, Integer> map : person1.getAction().entrySet()){
                    System.out.println(map.getKey() + " " + map.getValue());
                }
                for (Map.Entry<String, Integer> map : person2.getAction().entrySet()){
                    System.out.println(map.getKey() + " " + map.getValue());
                }
                for (Map.Entry<String, Integer> map : person3.getAction().entrySet()){
                    System.out.println(map.getKey() + " " + map.getValue());
                }

                System.out.println(account.getAmount());
            }
        }
    }
}
