import java.util.Random;

/**
 * Created by forte on 5.4.17.
 */
public class Producer extends Person implements Runnable {
    private int upperboundAmount;
    private int lowerboundAmount;
    private int upperboundTimes;
    private int lowerboundTimes;
    private int nTimes;
    private Random rand = new Random(3);

    public Producer(Account account, int upperboundAmount, int lowerboundAmount, int upperboundTimes, int lowerboundTimes, int nTimes) {
        this.account = account;
        this.upperboundAmount = upperboundAmount;
        this.lowerboundAmount = lowerboundAmount;
        this.upperboundTimes = upperboundTimes;
        this.lowerboundTimes = lowerboundTimes;
        this.nTimes = nTimes;
    }

    public void add(int n) {
        int value = n + account.getAmount();
        account.setAmount(value);
        action.put("add", value);
    }

    @Override
    public void run() {

        while (nTimes-- > 0) {
            int randomAmount = rand.nextInt(upperboundAmount-lowerboundAmount) + lowerboundAmount;
            int randomSleep = rand.nextInt(upperboundTimes-lowerboundTimes) + lowerboundTimes;


            add(randomAmount);
            try {
                Thread.sleep(randomSleep);
            } catch (InterruptedException ignored) {

            }
        }

    }
}
