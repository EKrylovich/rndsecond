import java.util.Random;

/**
 * Created by forte on 5.4.17.
 */
public class Account {
    private Random rand = new Random(3);
    volatile int amount = rand.nextInt(16);
    private int initialAmount;
    {
        initialAmount = amount;
    }

    public synchronized int getAmount() {
        return amount;
    }

    public synchronized void setAmount(int amount) {
        this.amount = amount;
    }

    public int getInitialAmount() {
        return initialAmount;
    }
}
