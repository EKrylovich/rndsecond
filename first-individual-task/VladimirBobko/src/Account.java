import java.util.Random;

public class Account {

    public static long account;
    static {
        Random random = new Random(16);
        account = random.nextLong();
    }

    public static void increm(int a){
        account += a;
    }

    public static long getAccount() {
        return account;
    }
}
