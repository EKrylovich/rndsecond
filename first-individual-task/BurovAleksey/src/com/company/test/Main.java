package com.company.test;

import com.company.classes.ConsumerPerson;
import com.company.classes.FirstPersonImpl;
import com.company.classes.Person;
import com.company.classes.SecondPersonImpl;

public class Main {

    public static void main(String[] args) {
	// write your code here
        new Thread(new FirstPersonImpl()).start();
        new Thread(new SecondPersonImpl()).start();
        new Thread(new ConsumerPerson()).start();

    }
}
