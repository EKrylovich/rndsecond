package com.company.classes;

/**
 * Created by alex on 5.4.17.
 */
public class ConsumerPerson extends Person {


    @Override
    public void run() {
        while (!Thread.interrupted()){
            synchronized (Account.lock) {
               while (Account.getSum() < 10) {
                   try {
                       Account.lock.wait();
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
               Account.draw(rnd(8, 10));

            }
        }

    }
}
