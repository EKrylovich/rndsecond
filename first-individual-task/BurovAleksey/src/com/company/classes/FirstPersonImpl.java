package com.company.classes;

/**
 * Created by alex on 5.4.17.
 */
public class FirstPersonImpl extends Person {
    private final int times;
    public FirstPersonImpl() {
        times = 50;
    }

    @Override
    public void run() {
        for(int i = 0; i < times; i++){
            synchronized (Account.lock) {
                Account.addSum(rnd(2, 5));
                try {
                    Thread.sleep(rnd(1, 3) * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(Account.getSum() > 10)
                    Account.lock.notifyAll();
            }
        }
    }

}
