package com.company.classes;

import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by alex on 5.4.17.
 */
public class Account {
    private static volatile AtomicLong sum = new AtomicLong((long) Math.random() * 15);
    private static final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
    public static Object lock = new Object();




    public static void addSum(long x){

           long old = sum.get();
           sum.set(old + x);
           printDetails("Add",x,sum.get());

    }

    public static void draw(long x){

            long old = sum.get();
            sum.set(old + x);
            printDetails("Draw",x,sum.get());

    }


    public static void printDetails(String type,long sum,long acountsum){
        String time = format.format(System.currentTimeMillis());
        System.out.printf("%s %s Amount: %d, Account Amount : %d\n",time,type,sum,acountsum);
    }

    public static long getSum() {
        return sum.get();
    }
}
