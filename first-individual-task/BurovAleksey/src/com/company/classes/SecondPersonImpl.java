package com.company.classes;

/**
 * Created by alex on 5.4.17.
 */
public class SecondPersonImpl extends Person {
    private final int times;

    public SecondPersonImpl() {
        times = 30;
    }

    @Override
    public void run() {
        for(int i = 0; i < times; i++){
            synchronized (Account.lock) {
                Account.addSum(rnd(3, 6));
                try {
                    Thread.sleep(rnd(2, 4) * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(Account.getSum() > 10)
                    Account.lock.notifyAll();
            }
        }
    }
}
