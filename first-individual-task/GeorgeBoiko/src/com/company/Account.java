package com.company;

import java.util.Random;

/**
 * Created by PC on 05.04.2017.
 */
public class Account {
    private int amount;
    private Random random;

    public Account() {
        random = new Random();
        amount = random.nextInt(16);
    }

    public synchronized void put(int personAmount) {
        System.out.println("init amount " + amount+" ;put number " + personAmount);
        while ((amount + personAmount) > 10) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        amount = amount + personAmount;
        System.out.println("final amount " + amount);
        notifyAll();
    }

    public synchronized void get() {
        int a = random.nextInt(2) + 8;
        while(amount - a < -10) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        amount = amount - a;
        System.out.println("get number "+ a + " ;final amount after get " + amount);
        notifyAll();
    }
}
