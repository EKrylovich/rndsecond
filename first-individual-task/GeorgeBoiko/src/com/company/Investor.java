package com.company;

import java.util.Random;

/**
 * Created by PC on 05.04.2017.
 */
public class Investor implements Runnable {
    private Account account;

    public Investor(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        Random random = new Random();
        int second;
        while (true) {
            second = random.nextInt(61);
            if (second % 2 != 0) {
                for (int i = 1; i < 50; i++) {
                    account.put(random.nextInt(3) + 2);
                }
            } else
                for (int i = 1; i < 30; i++) {
                    account.put(random.nextInt(3) + 3);
                }
        }
    }
}
