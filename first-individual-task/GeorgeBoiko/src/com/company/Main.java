package com.company;


public class Main {

    public static void main(String[] args) {
	// write your code here
        Account account = new Account();
        Investor investor1 = new Investor(account);
        Investor investor2 = new Investor(account);
        Recipient recipient = new Recipient(account);
        new Thread(investor1).start();
        new Thread(investor2).start();
        new Thread(recipient).start();


    }
}
