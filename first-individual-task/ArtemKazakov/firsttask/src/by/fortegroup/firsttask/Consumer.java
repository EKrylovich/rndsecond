package by.fortegroup.firsttask;

import java.time.LocalDateTime;
import java.util.Random;

public class Consumer implements Runnable {

    private Account account;
    private Random random;
    private int minAmount;
    private int minGet;
    private int maxGet;
    private boolean isWorking = true;

    public Consumer(Account account, int minAmount, int minGet, int maxGet){
        random = new Random();
        this.account = account;
        this.minAmount = minAmount;
        this.minGet = minGet;
        this.maxGet = maxGet;
    }

    @Override
    public void run() {
        synchronized (account){
            while(isWorking) {
                while (account.getAmount() <= minAmount) {
                    try {
                        account.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                int gettingAmount = (random.nextInt(maxGet - minGet) + minGet);
                account.setAmount(account.getAmount() - gettingAmount);
                System.out.printf("Time: %s, Type: get, Amount: %d, Account amount: %d\n",
                        LocalDateTime.now().toString(), gettingAmount, account.getAmount());
            }
        }
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }
}
