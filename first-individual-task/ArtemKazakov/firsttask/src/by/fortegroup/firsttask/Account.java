package by.fortegroup.firsttask;

import java.util.Random;

public class Account {

    private int amount;

    public Account(){
        Random random = new Random();
        amount = random.nextInt(16);
        System.out.printf("Initial amount: %d\n", amount);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
