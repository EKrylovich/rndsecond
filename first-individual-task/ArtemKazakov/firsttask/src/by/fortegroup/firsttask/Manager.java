package by.fortegroup.firsttask;

public class Manager implements Runnable {

    private boolean isWorking = true;
    private Integer workingCount;

    @Override
    public void run() {
        Account account = new Account();
        Thread firstProducer = new Thread(new Producer(account, 2, 5, 50, 1, 3, workingCount));
        Thread secondProducer = new Thread(new Producer(account, 3, 6, 30, 2, 4, workingCount));
        Consumer consumerThread = new Consumer(account, 10, 8, 10);
        Thread consumer = new Thread(consumerThread);
        firstProducer.start();
        secondProducer.start();
        consumer.start();

        this.workingCount = 2;

        while (isWorking){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (firstProducer.isAlive() && secondProducer.isAlive());
        }

        synchronized (workingCount){
            while (workingCount != 0){
                try {
                    workingCount.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            consumerThread.setWorking(false);
        }
    }
}
