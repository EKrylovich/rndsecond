package by.fortegroup.firsttask;

import java.time.LocalDateTime;
import java.util.Random;

public class Producer implements Runnable {

    private Account account;
    private Random random;
    private int minAdd;
    private int maxAdd;
    private int count;
    private int minWait;
    private int maxWait;
    private Integer workingCount;

    public Producer(Account account, int minAdd, int maxAdd, int count, int minWait, int maxWait, Integer workingCount) {
        this.account = account;
        random = new Random();
        this.minAdd = minAdd;
        this.maxAdd = maxAdd;
        this.count = count;
        this.minWait = minWait;
        this.maxWait = maxWait;
        this.workingCount = workingCount;
    }


    @Override
    public void run() {
        for (int i = 0; i < count; i++) {
            try {
                Thread.sleep((random.nextInt(maxAdd-minAdd) + minAdd) * 1000);
            } catch (InterruptedException e) {
                 e.printStackTrace();
            }
            synchronized (account) {
                int addingAmount = random.nextInt(maxWait - minWait) + minWait;
                account.setAmount(account.getAmount() + addingAmount);
                System.out.printf("Time: %s, Type: add, Amount: %d, Account amount: %d\n",
                        LocalDateTime.now().toString(), addingAmount, account.getAmount());
                account.notifyAll();
            }
        }
        System.out.printf("Thread %s finished work. Was added: %d\n", Thread.currentThread().getName(), count);
        synchronized (workingCount){
            workingCount--;
        }
    }
}
