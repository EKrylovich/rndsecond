public class Account {
    private final long initialAmount;
    private long currentAmount;

    public Account(){
        initialAmount = (long) (Math.random() * 15);
        currentAmount = initialAmount;
    }

    public long getCurrentAmount(){
        return this.currentAmount;
    }

    public long getInitialAmount(){
        return initialAmount;
    }

    public void add(long amount){
        this.currentAmount += amount;
    }

    public long get(long amount){
        if(this.currentAmount >= amount){
            this.currentAmount -= amount;
            return amount;
        }

        return 0;
    }
}
