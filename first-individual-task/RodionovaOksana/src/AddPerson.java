public class AddPerson implements Runnable{
    private final Account account;
    private final long minAmount;
    private final long maxAmount;
    private int times;
    private int minDelay;
    private int maxDelay;

    public AddPerson(Account account, long min, long max){
        this.account = account;
        this.minAmount = min;
        this.maxAmount = max;
    }

    public void setTimes(int times){
        this.times = times;
    }

    public void setDelay(int min, int max){
        this.minDelay = min;
        this.maxDelay = max;
    }

    public void run(){
        synchronized(account){
            int curTimes = 0;
            while(curTimes <= times) {
                account.add((long) (Math.random() * (maxAmount - minAmount) + minAmount));
                try {
                    wait((long) (Math.random() * (maxDelay - minDelay) + minDelay));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
