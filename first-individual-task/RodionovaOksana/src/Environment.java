public class Environment {
    private Account account = new Account();

    public static void main(String[] args){
        new Environment().accountChanging();
    }

    private void accountChanging(){
        AddPerson addPerson1 = new AddPerson(account, 2L, 5L);
        addPerson1.setDelay(1, 3);
        addPerson1.setTimes(50);
        AddPerson addPerson2 = new AddPerson(account, 3L, 6L);
        addPerson2.setDelay(2, 4);
        addPerson2.setTimes(30);
        GetPerson getPerson = new GetPerson(account, 8, 10);
        new Thread(addPerson1).start();
        new Thread(addPerson2).start();
        new Thread(getPerson).start();
    }
}
