public class GetPerson implements Runnable{
    private final Account account;
    private final long minAmount;
    private final long maxAmount;

    public GetPerson(Account account, long min, long max){
        this.account = account;
        this.minAmount = min;
        this.maxAmount = max;
    }

    public void run(){
        synchronized (account){
            try{
                while(account.getCurrentAmount() <= maxAmount){
                    wait();
                }
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
            account.get((long) (Math.random()*(maxAmount - minAmount) + minAmount));
        }
    }
}
