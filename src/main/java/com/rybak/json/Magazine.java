package com.rybak.json;

public class Magazine extends Literature {

    private boolean colored;

    public Magazine(String title, String type, boolean colored) {
        super.setTitle(title);
        super.setType(type);
        this.colored = colored;
    }

    public boolean isColored() {
        return colored;
    }

    public void setColored(boolean colored) {
        this.colored = colored;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Magazine magazine = (Magazine) o;

        return colored == magazine.colored;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (colored ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "colored=" + colored +
                "} " + super.toString();
    }
}
