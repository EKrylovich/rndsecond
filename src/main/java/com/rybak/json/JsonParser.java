package com.rybak.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonParser {

    private File inputJson;
    private File outputJson;

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public JsonParser(File inputJson) {
        this.inputJson = inputJson;
        this.outputJson = new File(this.inputJson.getParent()+"\\output.json");
    }

    private List<Book> getBooks() throws FileNotFoundException {

        List<Book> books = gson.fromJson(new FileReader(inputJson), new TypeToken<List<Book>>(){}.getType());

        return books;
    }

    private List<Magazine> getMagazines() throws FileNotFoundException {


        List<Magazine> magazines = gson.fromJson(new FileReader(inputJson), new TypeToken<List<Magazine>>(){}.getType());

        return magazines;
    }
    private List<Book> parseBooks() throws FileNotFoundException {
        List<Book> inputBooks = getBooks();

        List<Book> outputBooks = new ArrayList<>();

        for(Book book:inputBooks) {
            if(book.getType() == null) {
                continue;
            } else if(book.getType().equals("book") && book.getPageCount() > 100) {
                outputBooks.add(book);
            }
        }
        return outputBooks;
    }

    private List<Magazine> parseMagazines() throws FileNotFoundException {
        List<Magazine> inputMagazines = getMagazines();

        List<Magazine> outputMagazines = new ArrayList<>();

        for(Magazine magazine:inputMagazines) {
            if(magazine.getType() == null) {
                continue;
            } else if(magazine.getType().equals("magazin") && magazine.isColored()) {
                outputMagazines.add(magazine);
            }
        }
        return outputMagazines;
    }

    public void parseAll() throws IOException {
        List<Magazine> outputMagazines = parseMagazines();
        List<Book> outputBooks = parseBooks();

        try (FileWriter writer = new FileWriter(outputJson)) {

            gson.toJson(outputMagazines, writer);

        }
        try (FileWriter writer = new FileWriter(outputJson)) {

            gson.toJson(outputMagazines, writer);

        }

    }


}
