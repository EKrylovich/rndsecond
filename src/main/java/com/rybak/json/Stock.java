package com.rybak.json;


import java.util.List;

public class Stock {

    private List<Literature> list;

    public Stock(List<Literature> list) {
        this.list = list;
    }

    public List<Literature> getList() {
        return list;
    }

    public void setList(List<Literature> list) {
        this.list = list;
    }
}
