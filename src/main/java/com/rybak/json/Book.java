package com.rybak.json;

public class Book extends Literature {

    private int pageCount;

    public Book(String title, String type, int pageCount) {
        super.setTitle(title);
        super.setType(type);
        this.pageCount = pageCount;
    }

    public Book(Literature literature) {

    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        return pageCount == book.pageCount;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + pageCount;
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "pageCount=" + pageCount +
                "} " + super.toString();
    }
}
