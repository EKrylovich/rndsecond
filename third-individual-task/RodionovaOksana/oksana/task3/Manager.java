package oksana.task3;

import oksana.task3.model.Address;
import oksana.task3.model.Building;
import oksana.task3.model.Street;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
public class Manager {
    private Set<Building> buildings;
    private Set<Street> streets;
    private Set<Address> addresses;

    public Manager(){
        buildings = new TreeSet<>();
        streets = new TreeSet<>();
        addresses = new TreeSet<>();
    }

    public void addBuilding(Building pBuilding, Street street){
        buildings.add(pBuilding);
    }

    public void addStreet(Street pStreet){
        streets.add(pStreet);
    }

    public Set<Building> getBuildings(){
        return new TreeSet<>(buildings);
    }

    public Set<Street> getStreets(){
        return new TreeSet<>(streets);
    }

    public void editBuilding(Building existBuilding, Building newBuilding){
        if(buildings.contains(existBuilding)){
            buildings.remove(existBuilding);
            buildings.add(newBuilding);
        }
    }

    public void editStreet(Street existStreet, Street newStreet){

    }

    public void removeBuilding(int id){

    }

    public void removeStreet(int id){

    }
}
