package oksana.task3;

/**
 * Prints log messages
 * */
public class LogTool {
    public static void infoLog(String pMessage){
        System.out.println(pMessage);
    }

    public static void errorLog(Throwable pError, String pMessage){
        infoLog(pMessage);
        if(pError != null){
            pError.printStackTrace();
        }
    }
}
