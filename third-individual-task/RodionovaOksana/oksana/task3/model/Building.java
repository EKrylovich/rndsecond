package oksana.task3.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Represents model of Building
 */
public class Building{
    private int number;
    private Map<Street, List<Address>> addresses;

    public Building(int pNumber) {
        number = pNumber;
        addresses = new ConcurrentHashMap<>();
    }

    public Building(Building pBuilding){
        number = pBuilding.getNumber();
        addresses = new ConcurrentHashMap<>(pBuilding.getAddresses());
    }

    public int getNumber(){
        return number;
    }

    public Map<Street, List<Address>> getAddresses(){
        Map<Street, List<Address>> addressesCopy = new ConcurrentHashMap<>();
        for(Street street : addresses.keySet()){
            addressesCopy.put(street, new ArrayList<>(addresses.get(street)));
        }

        return addressesCopy;
    }

    @Override
    public int hashCode() {
        return 31 * number + addresses.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual;
        if(this == obj){
            isEqual = true;
        }
        else
            if(this.getClass() != obj.getClass()) {
                isEqual = false;
            }
            else {
                Building otherBuilding = (Building) obj;
                isEqual = this.getNumber() == otherBuilding.getNumber() && this.getAddresses().equals(otherBuilding.getAddresses());
            }

        return isEqual;
    }
}
