package oksana.task3.model;

/**
 * Represents model of Address
 */
public class Address {
    private Building building;
    private Street street;

    public Address(Building pBuilding, Street pStreet){
        building = pBuilding;
        street = pStreet;
    }

    public Building getBuilding(){
        return new Building(building);
    }

    public Street getStreet(){
        return new Street(street);
    }

    @Override
    public int hashCode() {
        return 31 * building.hashCode() + street.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual;
        if(this == obj){
            isEqual = true;
        }
        else
        if(this.getClass() != obj.getClass()) {
            isEqual = false;
        }
        else {
            Address otherAddress = (Address) obj;
            isEqual = this.getBuilding().equals(otherAddress.getBuilding()) && this.getStreet().equals(otherAddress.getStreet());
        }

        return isEqual;
    }
}
