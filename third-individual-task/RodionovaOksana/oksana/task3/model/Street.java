package oksana.task3.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents model of Street
 */
public class Street {
    private String name;
    private Map<Building, List<Address>> addresses;

    public Street(String pName){
        name = pName;
        addresses = new ConcurrentHashMap<>();
    }

    public Street(Street pStreet){
        name = pStreet.getName();
        addresses = new ConcurrentHashMap<>(pStreet.getAddresses());
    }

    public String getName(){
        return name;
    }

    public Map<Building, List<Address>> getAddresses(){
        Map<Building, List<Address>> addressesCopy = new ConcurrentHashMap<>();
        for(Building building : addresses.keySet()){
            addressesCopy.put(building, new ArrayList<>(addresses.get(building)));
        }

        return addressesCopy;
    }

    public int hashCode() {
        return 31 * name.hashCode() + addresses.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual;
        if(this == obj){
            isEqual = true;
        }
        else
        if(this.getClass() != obj.getClass()) {
            isEqual = false;
        }
        else {
            Street otherStreet = (Street) obj;
            isEqual = this.getName().equals(otherStreet.getName()) && this.getAddresses().equals(otherStreet.getAddresses());
        }

        return isEqual;
    }
}
