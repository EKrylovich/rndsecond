package oksana.task3;

import oksana.task3.model.Building;
import oksana.task3.model.Street;

/**
 *
 */
public class Main {
    public static void main(String[] args){
        Manager manager = new Manager();

        new Thread(){
            @Override
            public void run() {
                for(int i = 0; i < 20; i++){
                    manager.addBuilding(new Building(i), new Street("Street" + i));
                }
            }
        }.start();

        for (Building building : manager.getBuildings()){
            LogTool.infoLog(building.toString());
        }

        new Thread(){
            @Override
            public void run() {
                for(int i = 0; i < 15; i++){
                    manager.editStreet(new Street("Street" + i), new Street("Street" + (i + 1)));
                }
            }
        }.start();
    }
}
