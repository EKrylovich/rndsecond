package by.burov.thirdtask;

import by.burov.thirdtask.model.Address;
import by.burov.thirdtask.model.Building;
import by.burov.thirdtask.model.Street;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by alex on 19.4.17.
 */
public class Manager {
    Set<Building> buildings;
    Set<Street> streets;

    public Manager() {
        buildings = new HashSet<>();
        streets = new HashSet<>();
    }


    public Building getBuilding(Building building){
        for(Building current : buildings){
            if(current.equals(building))
                return current;
        }
        return null;
    }

    public boolean addBuilding(Address address, Building building) {
      if(buildings.contains(building))
          return false;

      synchronized (buildings){
          synchronized (streets){
              Iterator<Building> iterator = buildings.iterator();
              Building current = null;
              while (iterator.hasNext()){
                  current = iterator.next();
                  if(current.equals(building)) {
                      break;
                  }else {
                      current = null;
                  }
              }

              if(current != null){
                  current.getAddresses().add(address);
                  buildings.add(building);

              }
          }
      }

    }

    public void removeBuilding(Building building){
        buildings.remove(building);
    }

    public void updateBuilding(Building building){
        if(buildings.contains(building))
            buildings.add(building);
    }


    public boolean addStreet(Street street){
        if(streets.contains(street)){
            return false;
        }

        synchronized (streets){
           streets.add(street);
           return true;
        }
    }

    public boolean deleteStreet(Street street){
        if(!streets.contains(street))
            return false;
        synchronized (streets){
            streets.remove(street);
            for(Building building : buildings){
                Iterator<Address> addesses = building.getAddresses().iterator();
                while (addesses.hasNext()) {
                    Address current = addesses.next();
                    if(current.getStreet().equals(street))
                        addesses.remove();
                }
            }
        }

    }

    public void updateStreet(Street street){
        
    }
}
