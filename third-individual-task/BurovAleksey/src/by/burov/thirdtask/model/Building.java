package by.burov.thirdtask.model;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Simple model class for Building entity
 * @author Alexey Burov
 */
public class Building {
    private Set<Address> addresses;

    public Building() {
        addresses = new ConcurrentSkipListSet<>();
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        return addresses != null ? addresses.equals(building.addresses) : building.addresses == null;
    }

    @Override
    public int hashCode() {
        return addresses != null ? addresses.hashCode() : 0;
    }
}
