package by.burov.thirdtask.model;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Simple class it represent Street entity
 * @author  Alexey Burov
 */
public class Street {
    private String name;
    private Map<Address,Building> buildings;

    public Street(String name) {
        this.name = name;
        buildings = new ConcurrentHashMap<>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Map<Address, Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(Map<Address, Building> buildings) {
        this.buildings = buildings;
    }
}
