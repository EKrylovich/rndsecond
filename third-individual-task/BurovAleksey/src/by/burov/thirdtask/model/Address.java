package by.burov.thirdtask.model;

/**
 * Created by alex on 19.4.17.
 */
public class Address {

    private Street street;
    private int number;

    public Address(Street street, int number) {
        this.street = street;
        this.number = number;
    }

    public Street getStreet() {
        return street;
    }

    public int getNumber() {
        return number;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (number != address.number) return false;
        return street != null ? street.equals(address.street) : address.street == null;
    }

    @Override
    public int hashCode() {
        int result = street != null ? street.hashCode() : 0;
        result = 31 * result + number;
        return result;
    }
}
