package com.forte;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ux501 on 19.04.2017.
 */
public class Manager {

    private Set<Building> buildingSet = new HashSet<>();
    private Set<Address> addressSet = new HashSet<>();
    private Set<Street> streetSet = new HashSet<>();

    static AtomicInteger buildingCounter = new AtomicInteger(0);
    static AtomicInteger streetCounter = new AtomicInteger(0);

    public synchronized void create(Address address) {
        if (addressSet.add(address)) {
            Building building = address.getBuilding();
            Street street = address.getStreet();

            List<Address> bList = building.getMap().get(street);
            if (bList != null) {
                bList.add(address);
            } else {
                bList = new ArrayList<>();
                bList.add(address);
                building.getMap().put(street, bList);
            }

            List<Address> sList = street.getMap().get(building);
            if (sList != null) {
                sList.add(address);
            } else {
                sList = new ArrayList<>();
                sList.add(address);
                street.getMap().put(building, sList);
            }
        }
    }

    public synchronized void remove(Address address) {
        if (addressSet.remove(address)) {
            Building building = address.getBuilding();
            Street street = address.getStreet();

            long buildingId = building.getBuildingId();
            List<Address> bList = null;
            for (Building build : buildingSet) {
                if (build.getBuildingId() == buildingId) {
                    Map<Street, List<Address>> map = build.getMap();
                    bList = map.get(street);
                    break;
                }
            }
            if (bList != null) {
                bList.remove(address);
            }


            long streetId = street.getStreetId();
            List<Address> sList = null;
            for (Street str : streetSet) {
                if (str.getStreetId() == streetId) {
                    Map<Building, List<Address>> map = street.getMap();
                    sList = map.get(building);
                    break;
                }
            }
            if (sList != null) {
                sList.remove(address);
            }

        }
    }

    public synchronized Address getAddress(String addressName) throws IOException, ClassNotFoundException {
        Address address = null;
        for (Address addr : addressSet) {
            if (addr.getAddressName().equals(addressName)) {
                address = addr;
                break;
            }
        }

        if (address != null) {
            return (Address) clone(address);
        }

        return null;
    }

    public Object clone(Object object) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(object);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        return objectInputStream.readObject();

    }

    public synchronized List<Street> searchByPartOfSteetName(String streetNamePart) {
        List<Street> streets = new ArrayList<>();
        for (Street street : streetSet) {
            if (street.getStreetName().contains(streetNamePart)) {
                streets.add(street);
            }
        }
        return streets;
    }

    public synchronized List<Building> searchByStrNameAndBuildRange(String streetNamePart, int lowBuildNumber,
                                                                    int highBuildNumber) {

        List<Building> buildings = new ArrayList<>();
        List<Street> streets = searchByPartOfSteetName(streetNamePart);
        for (Street street : streets) {
            Map<Building, List<Address>> map = street.getMap();
            for (Building building : map.keySet()) {
                if (building.getBuilingNumber() >= lowBuildNumber && building.getBuilingNumber() <= highBuildNumber) {
                    buildings.add(building);
                }
            }

        }
        return buildings;
    }


    public static void main(String[] args) {
        Manager manager = new Manager();
        Address address = new Address(new Building(buildingCounter.incrementAndGet(), 45),
                new Street(streetCounter.incrementAndGet(), "firstStreet"), "Kosmonavtov");

        manager.create(address);

        manager.addressSet.forEach(s -> {
            LogTools.log(s.toString());
        });


    }

}
