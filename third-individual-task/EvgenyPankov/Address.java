package com.forte;

/**
 * Created by ux501 on 19.04.2017.
 */
public class Address {

    private Building building;
    private Street street;
    private String addressName;

    public Address(Building building, Street street, String addressName) {
        this.building = building;
        this.street = street;
        this.addressName = addressName;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        if (!building.equals(address1.building)) return false;
        if (!street.equals(address1.street)) return false;
        return addressName.equals(address1.addressName);
    }

    @Override
    public int hashCode() {
        int result = building.hashCode();
        result = 31 * result + street.hashCode();
        result = 31 * result + addressName.hashCode();
        return result;
    }
}
