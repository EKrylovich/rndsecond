package com.forte;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ux501 on 19.04.2017.
 */
public class Street {

    private long streetId;
    private String streetName;
    Map<Building, List<Address>> map = new HashMap<>();

    public Street(long streetId, String streetName) {
        this.streetId = streetId;
        this.streetName = streetName;
    }

    public long getStreetId() {
        return streetId;
    }

    public String getStreetName() {
        return streetName;
    }

    public Map<Building, List<Address>> getMap() {
        return map;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setMap(Map<Building, List<Address>> map) {
        this.map = map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Street street = (Street) o;

        return streetId == street.streetId;
    }

    @Override
    public int hashCode() {
        return (int) (streetId ^ (streetId >>> 32));
    }
}
