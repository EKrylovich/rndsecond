package com.forte;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ux501 on 19.04.2017.
 */
public class Building {

    private long buildingId;
    private int builingNumber;
    private Map<Street, List<Address>> map = new HashMap<>();

    public Building(long buildingId, int builingNumber) {
        this.buildingId = buildingId;
        this.builingNumber = builingNumber;
    }

    public long getBuildingId() {
        return buildingId;
    }

    public int getBuilingNumber() {
        return builingNumber;
    }

    public void setBuilingNumber(int builingNumber) {
        this.builingNumber = builingNumber;
    }

    public Map<Street, List<Address>> getMap() {
        return map;
    }

    public void setMap(Map<Street, List<Address>> map) {
        this.map = map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        return buildingId == building.buildingId;
    }

    @Override
    public int hashCode() {
        return (int) (buildingId ^ (buildingId >>> 32));
    }
}
