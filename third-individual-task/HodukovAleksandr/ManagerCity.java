package com.task3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ManagerCity {
	private Map<String, Set<Integer>> mapStreets;
	private Map<Integer, Set<String>> mapBuildings;
	

	public ManagerCity(Map<String, Set<Integer>> mapStreets,
			Map<Integer, Set<String>> mapBuildings) {
		super();
		this.mapStreets = mapStreets;
		this.mapBuildings = mapBuildings;
	}

	public void addAddress(Address address) {
		String street = address.getStreet();
		Integer building = address.getBuilding();
		
		if( !mapStreets.containsKey(street) ) {
			mapStreets.put(street, new HashSet<>());
		} 
		mapStreets.get(street).add(building);
		if( !mapBuildings.containsKey(building) ) {
			mapBuildings.put(building, new HashSet<>());
		} 
		mapBuildings.get(building).add(street);
		
		
	}
	
	public void updateAddress(Street street) {
		
	}
	
	public void removeAddress(Address address) {
		
	}
	
	public void removeStreet(String street) {
		
	}
	
	public List<String> searchByStreet(String streetPart) {
		Set<String> list = mapStreets.keySet();
		List<String> result = new ArrayList<>();
		
		for (String street : list) {
			if ( street.contains(streetPart)) {
				result.add(street);
			}
		}
		
		return result;
	}
	
	public List<Address> searchByStreetAndBuilding(String streetPart, Integer building) {
		List<Address> result = new ArrayList<>();
		return result;
	}
}
