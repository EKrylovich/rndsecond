package com.fortegroup.thirdtask;


import com.fortegroup.thirdtask.util.LogTool;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class Manager {
    private Set<Street> streets;
    private Set<Building> buildings;
    private Set<Address> addresses;

    public Manager(){
        streets = new CopyOnWriteArraySet<Street>();
        buildings = new CopyOnWriteArraySet<Building>();
        addresses = new CopyOnWriteArraySet<Address>();
    }

    public void addStreet(Street street){
        streets.add(street);
        for (Map.Entry<Building, List<Address>> entry : street.getBuildings().entrySet()){
            buildings.add(entry.getKey());
            for (Address address : entry.getValue()){
                addresses.add(address);
            }
        }
        for (Building building : street.getBuildings().keySet()){
            buildings.add(building);
        }
        LogTool.log("Street ", street.getName()," was added");
    }

    public void deleteStreet(Street street){
        streets.remove(street);
        for (Map.Entry<Building, List<Address>> entry : street.getBuildings().entrySet()){
            for (Address address : entry.getValue()){
                if (address.getStreet().equals(street)){
                    entry.getValue().remove(address);
                }
            }
        }

        for (Address address : addresses){
            if (address.getStreet().equals(street)){
                addresses.remove(address);
            }
        }

        LogTool.log("Street ", street.getName()," was deleted");
    }

    public Set<Street> findStreetsByPartOfName(String partOfName) throws CloneNotSupportedException{
        Set<Street> findStreets = new HashSet<Street>();
        for (Street street : streets){
            if (street.getName().contains(partOfName)){
                findStreets.add(street.clone());
            }
        }
        return findStreets;
    }

    public Set<Building> findBuildingsByPartOfNameAndRangeOfNumbers(String partOfName, int beginNum, int endNum)
            throws CloneNotSupportedException{
        Set<Building> findBuildings = new HashSet<Building>();
        for (Building building : buildings) {
            for (Map.Entry<Street, List<Address>> entry : building.getStreets().entrySet()) {
                if (entry.getKey().getName().contains(partOfName)) {
                    for (Address address : entry.getValue()) {
                        if (address.getNumber() >= beginNum && address.getNumber() <= endNum) {
                            findBuildings.add(building.clone());
                        }
                    }
                }
            }
        }
        return findBuildings;
    }
}
