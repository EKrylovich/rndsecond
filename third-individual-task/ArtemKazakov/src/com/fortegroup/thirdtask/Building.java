package com.fortegroup.thirdtask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Building implements Cloneable{

    private Map<Street, List<Address>> streets;

    public Building(){
        streets = new HashMap<Street, List<Address>>();
    }

    public Map<Street, List<Address>> getStreets() {
        return streets;
    }

    public void setStreets(Map<Street, List<Address>> streets) {
        this.streets = streets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Building building = (Building) o;

        return streets != null ? streets.equals(building.streets) : building.streets == null;

    }

    @Override
    public int hashCode() {
        return streets != null ? streets.hashCode() : 0;
    }

    public Building clone() throws CloneNotSupportedException{
        Building building = (Building)super.clone();
        return building;
    }
}
