package com.fortegroup.thirdtask;


public class Address implements Cloneable{
    private int number;
    private Street street;
    private Building building;

    public Address(int number){
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (number != address.number) return false;
        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        return building != null ? building.equals(address.building) : address.building == null;

    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (building != null ? building.hashCode() : 0);
        return result;
    }

    public Address clone() throws CloneNotSupportedException{
        Address address = (Address)super.clone();
        return address;
    }
}
