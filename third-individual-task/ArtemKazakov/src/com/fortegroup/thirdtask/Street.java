package com.fortegroup.thirdtask;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Street implements Cloneable{
    private String name;

    private Map<Building, List<Address>> buildings;

    public Street(String name){
        this.name =name;
        buildings = new HashMap<Building, List<Address>>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Building, List<Address>> getBuildings() {
        return buildings;
    }

    public void setBuildings(Map<Building, List<Address>> buildings) {
        this.buildings = buildings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Street street = (Street) o;

        if (name != null ? !name.equals(street.name) : street.name != null) return false;
        return buildings != null ? buildings.equals(street.buildings) : street.buildings == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (buildings != null ? buildings.hashCode() : 0);
        return result;
    }

    public Street clone() throws CloneNotSupportedException{
        Street street = (Street)super.clone();
        return street;
    }

}
